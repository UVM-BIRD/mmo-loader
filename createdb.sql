-- Copyright 2015 The University of Vermont and State Agricultural
-- College.  All rights reserved.
--
-- Written by Matthew B. Storer <matthewbstorer@gmail.com>
--
-- This file is part of MMO Loader.
--
-- MMO Loader is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- MMO Loader is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with MMO Loader.  If not, see <http:www.gnu.org/licenses/>.

create database if not exists mmo;

grant all on mmo.* to 'mmo'@'localhost' identified by 'mmo';

create table if not exists mmo.record (
  id int not null primary key,
  pmid int not null,
  type varchar(2) not null,
  index pmid(pmid),
  index type(type)
) engine MyISAM,
  character set latin1;

create table if not exists mmo.utterance (
  id int not null primary key,
  recordId int not null,
  sectionId int not null,
  utterance varchar(500) not null,
  index recordId(recordId)
) engine MyISAM,
  character set latin1;

create table if not exists mmo.utterancePos (
  utteranceId int not null,
  startPos int not null,
  spanLength int not null,
  index utteranceId(utteranceId)
) engine MyISAM,
  character set latin1;

create table if not exists mmo.phrase (
  partitionKey tinyint unsigned not null,
  id int not null,
  utteranceId int not null,
  phrase varchar(255) not null,
  primary key (id, partitionKey),
  index utteranceId(utteranceId),
  index phrase(phrase)
) engine MyISAM,
  character set latin1
  partition by hash(partitionKey)
  partitions 5;

create table if not exists mmo.phraseTag (
  partitionKey tinyint unsigned not null,
  phraseId int not null,
  tagId int not null,
  primary key (partitionKey, phraseId, tagId)
) engine MyISAM,
  character set latin1
  partition by hash(partitionKey)
  partitions 5;

create table if not exists mmo.tag (
  id int not null primary key,
  function varchar(10) not null,
  lexMatch varchar(255),
  inputMatch varchar(255),
  tag varchar(100),
  tokens varchar(255),
  features varchar(100)
) engine MyISAM,
  character set latin1;

create table if not exists mmo.phrasePos (
  partitionKey tinyint unsigned not null,
  phraseId int not null,
  startPos int not null,
  spanLength int not null,
  index phraseId(phraseId)
) engine MyISAM,
  character set latin1
  partition by hash(partitionKey)
  partitions 5;

create table if not exists mmo.mapping (
  partitionKey tinyint unsigned not null,
  id int not null,
  phraseId int not null,
  score int not null,
  primary key (id, partitionKey),
  index phraseId(phraseId)
) engine MyISAM,
  character set latin1
  partition by hash(partitionKey)
  partitions 5;

create table if not exists mmo.ev (
  partitionKey tinyint unsigned not null,
  id int not null,
  mappingId int not null,
  score int not null,
  umlsId varchar(11),
  concept varchar(255),
  conceptPrefName varchar(255),
  involvedWithHead tinyint(1),
  overmatch tinyint(1),
  primary key (id, partitionKey),
  index mappingId(mappingId),
  index umlsId(umlsId)
) engine MyISAM,
  character set latin1
  partition by hash(partitionKey)
  partitions 5;

create table if not exists mmo.evMatchedWord (
  partitionKey tinyint unsigned not null,
  evId int not null,
  word varchar(255),
  index evId(evId)
) engine MyISAM,
  character set latin1
  partition by hash(partitionKey)
  partitions 5;

create table if not exists mmo.evSemanticType (
  partitionKey tinyint unsigned not null,
  evId int not null,
  type varchar(255),
  index evId(evId)
) engine MyISAM,
  character set latin1
  partition by hash(partitionKey)
  partitions 5;

create table if not exists mmo.evMatchMap (
  partitionKey tinyint unsigned not null,
  evId int not null,
  phraseBegin int not null,
  phraseEnd int not null,
  conceptBegin int not null,
  conceptEnd int not null,
  variation int not null,
  index evId(evId)
) engine MyISAM,
  character set latin1
  partition by hash(partitionKey)
  partitions 5;

create table if not exists mmo.evSource (
  partitionKey tinyint unsigned not null,
  evId int not null,
  source varchar(255),
  index evId(evId)
) engine MyISAM,
  character set latin1
  partition by hash(partitionKey)
  partitions 5;

create table if not exists mmo.evPos (
  partitionKey tinyint unsigned not null,
  evId int not null,
  startPos int not null,
  spanLength int not null,
  index evId(evId)
) engine MyISAM,
  character set latin1
  partition by hash(partitionKey)
  partitions 5;

create table if not exists mmo.negation (
  partitionKey tinyint unsigned not null,
  id int not null,
  recordId int not null,
  negationType varchar(10) not null,
  negationTrigger varchar(255) not null,
  primary key (id, partitionKey),
  index recordId(recordId)
) engine MyISAM,
  character set latin1
  partition by hash(partitionKey)
  partitions 5;

create table if not exists mmo.negTriggerPos (
  partitionKey tinyint unsigned not null,
  negationId int not null,
  startPos int not null,
  spanLength int not null,
  index negationId(negationId)
) engine MyISAM,
  character set latin1
  partition by hash(partitionKey)
  partitions 5;

create table if not exists mmo.negConcept (
  partitionKey tinyint unsigned not null,
  negationId int not null,
  ncKey varchar(255) not null,
  ncValue varchar(255) not null,
  index negationId(negationId),
  index ncKey(ncKey)
) engine MyISAM,
  character set latin1
  partition by hash(partitionKey)
  partitions 5;

create table if not exists mmo.negConceptPos (
  partitionKey tinyint unsigned not null,
  negationId int not null,
  startPos int not null,
  spanLength int not null,
  index negationId(negationId)
) engine MyISAM,
  character set latin1
  partition by hash(partitionKey)
  partitions 5;

