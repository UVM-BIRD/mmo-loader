# MMO Loader #

MMO Loader is a Java-based command-line utility that may be used to download the latest [MetaMap Machine Output (MMO)](http://ii.nlm.nih.gov/MMBaseline/index.shtml) formatted results for MEDLINE/PubMed citations from the [National Library of Medicine](http://www.nlm.nih.gov/), and to load those data into a local relational database to facilitate advanced analysis. 

## Building MMO Loader ##

Building the MMO Loader is accomplished by running the following from the project's base directory:

    $ mvn package

## Rebuilding Grammars ##

Rebuilding MMO grammars is handled by the `build_grammar` shell script:

    $ ./build_grammar 
    Rebuilding MMO MetaMap Machine Output (MMO) lexer and parser -
    Done.

If changes are made to grammar files, you must rebuild grammars using `build_grammar` **before** building the MMO loader using `mvn package` as in the step above.

## Testing Grammars ##

Testing grammars is accomplished using the `test_grammar` shell script.  The script takes one parameter, the name of a file in the `test_files` folder.

When the test executes, it will run ANTLR's TestRig program against the appropriate file in the `test_files` folder, and will display a graphical representation of the file's generated abstract syntax tree.  This is a simple, useful way to test whether or not something is parsing correctly, and can quickly illustrate issues in the grammar.

Example:

    $ ./test_grammar example1.mmo
    Running TestRig -
    [@0,0:4='args(',<26>,1:0]
    [@1,5:21=''MetaMap Command'',<33>,1:5]
    [@2,22:23=',[',<11>,1:22]
    [@3,24:42='lexicon_year-'1234'',<30>,1:24]
    [@4,43:43=',',<4>,1:43]
    [@5,44:62='mm_data_year-'1234'',<30>,1:44]    
    ...

_Note that you must first compile project sources using `build_grammar` and `mvn package` before testing grammars._

## Preparation ##

Four preparatory steps must be taken before MMO Loader may be used.  These are obtaining a UMLS Metathesaurus License, creating the local database into which MMO data will be ultimately stored, ensuring you have enough disk space for downloaded files, and downloading the latest MMO files for processing.

### 1. Request a UMLS Metathesaurus License ###

The first step in getting the MMO Loader to work is to [request a UMLS Metathesaurus License](https://uts.nlm.nih.gov/license.html).  Requesting and obtaining such a license can take several business days.

### 2. Create Local Database ###

Before the MMO Loader can be used, the database into which MMO data will be stored must be created.  MMO Loader is pre-configured to use MySQL as its database back-end.  If you don't already have it installed, you should first download and install the [MySQL Community Server](https://dev.mysql.com/downloads/mysql/).

Once MySQL is installed, run the following command from the project's base directory:

    $ mysql -uroot < createdb.sql

This will create the _mmo_ database, with appropriate default values to integrate with the MMO Loader's default settings.

### 3. Ensure You Have Sufficient Free Disk Space ###

The MMO Loader's intermediate files and target database tables can take _several terabytes_ of disk space.  Please be sure that you have at least **2 terabytes** of disk space available before starting this process!

### 4. Download Raw MMO Files ###

After ensuring you have enough disk space available, you must download the raw data for the most recent year from [this page](http://ii.nlm.nih.gov/MMBaseline/index.shtml).  The link to click on this page will read _<YEAR> MetaMapped Medline Baseline Results_, where _<YEAR>_ is a valid year (e.g., 2015).

[Click here](http://mbr.nlm.nih.gov/Download/MetaMapped_Medline/2015/) to go directly to the 2015 data.  You will need to authenticate with your UMLS Metathesaurus credentials obtained in step 1 above to proceed.
 
Note that the remote file location specified above contains in addition to each individual data file (`text.out_*.gz`) a single tarball file `<YEAR>.tar` (where _<YEAR>_ is a valid year, e.g. `2015.tar`) that contains all result files for the specified year.  It is recommended to download this file instead of all individual files if you're going to use the full data set, and following steps will presume this action.

Once the full-year tarball has been downloaded, untar it into a directory, that hereafter will be referred to as `source_data`.

    $ mkdir source_data
    $ tar -xf 2015.tar -C source_data
    $ ls source_data
    text.out_01.gz	text.out_98.gz	text.out_195.gz	text.out_291.gz	text.out_388.gz	text.out_485.gz	text.out_582.gz	text.out_679.gz
    text.out_02.gz	text.out_99.gz	text.out_196.gz	text.out_292.gz	text.out_389.gz	text.out_486.gz	text.out_583.gz	text.out_680.gz
    text.out_03.gz	text.out_100.gz	text.out_197.gz	text.out_293.gz	text.out_390.gz	text.out_487.gz	text.out_584.gz	text.out_681.gz
    text.out_04.gz	text.out_101.gz	text.out_198.gz	text.out_294.gz	text.out_391.gz	text.out_488.gz	text.out_585.gz	text.out_682.gz
    ...

## Usage ##

To use the MMO Loader and to see a list of command-line switches, run the program without any arguments:

    $ java -jar mmo-loader-1.0.jar
    MEDLINE MMO Loader
    ------------------
    Copyright 2015 The University of Vermont and State Agricultural College.  All rights reserved.
    
    usage: load [-d <string>] [-h <string>] --load | --prepare <path> [-p <string>]  [-u <string>]
     -d,--db <string>      the database name (default 'mmo')
     -h,--host <string>    the database host (default 'localhost')
        --load             only load previously-prepared files into the target database
     -p,--pass <string>    the database user password (default 'mmo')
        --prepare <path>   only prepare database files for import from <path>
     -u,--user <string>    the database user name (default 'mmo')

### 1. Prepare MMO Files For Import ###

The MMO Loader has two primary modes of operation: `prepare` and `load`.  The preparation stage involves processing downloaded MMO files and transforming them such that they may be loaded into the target database.  The loading stage involves taking those prepared files and importing them into the target database.
 
To download and prepare files for import, run the following command:

    $ java -Xmx1250g -jar mmo-loader-1.0.jar --prepare source_data
    MEDLINE MMO Loader
    ------------------
    Copyright 2015 The University of Vermont and State Agricultural College.  All rights reserved.
    
    INFO  Load - process started at Mon Jun 15 16:40:55 EDT 2015
    INFO  AbstractFileLoader - [2]  (2/779, 0%)  processing 'text.out_02.gz'
    INFO  AbstractFileLoader - [1]  (1/779, 0%)  processing 'text.out_01.gz'
    INFO  AbstractFileLoader - [1]  (3/779, 0%)  processing 'text.out_03.gz'
    INFO  AbstractFileLoader - [2]  (4/779, 0%)  processing 'text.out_04.gz'
    INFO  AbstractFileLoader - [1]  (5/779, 0%)  processing 'text.out_05.gz'
    ...
    INFO  AbstractFileLoader - [2]  (778/779, 99%)  processing 'text.out_778.gz'
    INFO  AbstractFileLoader - [1]  (779/779, 100%)  processing 'text.out_779.gz'
    INFO  AbstractFileLoader - [2]  done.
    INFO  AbstractFileLoader - [1]  done.
    INFO  AbstractLoader - processing 779 files across 2 threads took 20 hours, 45 minutes, 1 second
    INFO  Load - preparing files from '/projects/genbank-loader/source_data/' finished at Tue Jun 16 13:25:57 EDT 2015 (took 20 hours, 45 minutes, 2 seconds).

Resultant files that will be imported into the local database are stored in the _out_ folder in the current working directory:

    $ ls out
    ev.txt			    ev_pos.txt		        mapping.txt		    neg_trigger_pos.txt	phrase_pos.txt		tag.txt
    ev_match_map.txt	ev_semantic_type.txt	neg_concept.txt		negation.txt		phrase_tag.txt		utterance.txt
    ev_matched_word.txt	ev_source.txt		    neg_concept_pos.txt	phrase.txt		    record.txt		    utterance_pos.txt
    
Each of these files is associated with each of the four tables in the target database.

#### Important Note Regarding Memory and Performance ####

MMO Loader is a multi-threaded process that can leverage the cores of your CPU to download and prepare MMO files in parallel, which can dramatically improve performance.

When MMO Loader executes its `prepare` function, it determines how much memory it has been allocated, and creates as many threads as it can safely use without negatively impacting other user and system processes.  MMO Loader will use at least one core, but may use up to (_N_-2) cores, where _N_ is the total number of cores in your system's CPU.

MMO Loader's preparation stage requires **500 megabytes of Java heap memory per thread** to properly execute, and may otherwise suffer severely degraded performance, or even fail with an `OutOfMemoryError`.  It is therefore suggested to allocate _at least 2.5 times_ the required memory per thread to the JVM, but more is better and will translate to improved performance, especially on systems with many CPU cores.

To allocate the minimum suggested memory to the GenBank Loader process, use the JVM option `-Xmx1250m` as in the example above.
 
See [Oracle's Java SE Documentation](http://docs.oracle.com/javase/7/docs/technotes/tools/windows/java.html) for details about `-Xmx` and other JVM options.

### 2. Load Prepared Files Into Local Database ###

To load the prepared MMO files into the local database, execute the following:

    $ java -jar mmo-loader-1.0.jar --load
    MEDLINE MMO Loader
    ------------------
    Copyright 2015 The University of Vermont and State Agricultural College.  All rights reserved.
    
    INFO  Load - process started at Tue Jun 16 17:01:20 EDT 2015
    INFO  AbstractLoader - populating database from './out' -
    INFO  AbstractLoader -  loading './out/record.txt' into table 'record'
    INFO  DataSource - getConnection : establishing connection to 'mmo'
    INFO  AbstractLoader -  building indexes for table 'record'
    INFO  AbstractLoader -  loading './out/utterance.txt' into table 'utterance'
    INFO  AbstractLoader -  building indexes for table 'utterance'
    INFO  AbstractLoader -  loading './out/utterance_pos.txt' into table 'utterancePos'
    INFO  AbstractLoader -  building indexes for table 'utterancePos'
    INFO  AbstractLoader -  loading './out/phrase.txt' into table 'phrase'
    INFO  AbstractLoader -  building indexes for table 'phrase'
    INFO  AbstractLoader -  loading './out/phrase_pos.txt' into table 'phrasePos'
    INFO  AbstractLoader -  building indexes for table 'phrasePos'
    INFO  AbstractLoader -  loading './out/tag.txt' into table 'tag'
    INFO  AbstractLoader -  building indexes for table 'tag'
    INFO  AbstractLoader -  loading './out/phrase_tag.txt' into table 'phraseTag'
    INFO  AbstractLoader -  building indexes for table 'phraseTag'
    INFO  AbstractLoader -  loading './out/mapping.txt' into table 'mapping'
    INFO  AbstractLoader -  building indexes for table 'mapping'
    INFO  AbstractLoader -  loading './out/ev.txt' into table 'ev'
    INFO  AbstractLoader -  building indexes for table 'ev'
    INFO  AbstractLoader -  loading './out/ev_matched_word.txt' into table 'evMatchedWord'
    INFO  AbstractLoader -  building indexes for table 'evMatchedWord'
    INFO  AbstractLoader -  loading './out/ev_semantic_type.txt' into table 'evSemanticType'
    INFO  AbstractLoader -  building indexes for table 'evSemanticType'
    INFO  AbstractLoader -  loading './out/ev_source.txt' into table 'evSource'
    INFO  AbstractLoader -  building indexes for table 'evSource'
    INFO  AbstractLoader -  loading './out/ev_match_map.txt' into table 'evMatchMap'
    INFO  AbstractLoader -  building indexes for table 'evMatchMap'
    INFO  AbstractLoader -  loading './out/ev_pos.txt' into table 'evPos'
    INFO  AbstractLoader -  building indexes for table 'evPos'
    INFO  AbstractLoader -  loading './out/negation.txt' into table 'negation'
    INFO  AbstractLoader -  building indexes for table 'negation'
    INFO  AbstractLoader -  loading './out/neg_trigger_pos.txt' into table 'negTriggerPos'
    INFO  AbstractLoader -  building indexes for table 'negTriggerPos'
    INFO  AbstractLoader -  loading './out/neg_concept.txt' into table 'negConcept'
    INFO  AbstractLoader -  building indexes for table 'negConcept'
    INFO  AbstractLoader -  loading './out/neg_concept_pos.txt' into table 'negConceptPos'
    INFO  AbstractLoader -  building indexes for table 'negConceptPos'
    INFO  AbstractLoader - finished populating database.  took 18 hours, 30 minutes, 26 seconds
    INFO  Load - populating database finished at Wed Jun 17 11:31:47 EDT 2015 (took 18 hours, 30 minutes, 27 seconds).
    INFO  DataSource - close : closing connection 'mmo'

Note that as soon as the `load` process completes, you may safely delete intermediate database import files in the _out_ folder.

## License and Copyright ##

MMO Loader is Copyright 2015 [The University of Vermont and State Agricultural College](https://www.uvm.edu/).  All rights reserved.

MMO Loader is licensed under the terms of the [GNU General Public License (GPL) version 3](https://www.gnu.org/licenses/gpl-3.0-standalone.html).

## ANTLR 4 License and Copyright ##

In order to simplify the building process of this project, ANTLR 4 binaries are included in this project.  In compliance with ANTLR licensing terms and conditions, the following is the ANTLR 4 license and copyright statement:

> **ANTLR 4 License**
>
> [The BSD License]
>
> Copyright (c) 2012 Terence Parr and Sam Harwell
> All rights reserved.
>
> Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
>
> * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
> * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
> * Neither the name of the author nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
>
> THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.