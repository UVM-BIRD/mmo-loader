// Copyright 2015 The University of Vermont and State Agricultural
// College.  All rights reserved.
//
// Written by Matthew B. Storer <matthewbstorer@gmail.com>
//
// This file is part of MMO Loader.
//
// MMO Loader is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// MMO Loader is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with MMO Loader.  If not, see <http:www.gnu.org/licenses/>.

grammar MMO;

init : args aas negList upcm+ EOF ;

upcm : utterance pcm+ '\'EOU\'.' ;

pcm : phrase candidates mappings ;


// args

args : 'args(' Str ',[' metaMapOptions ']).' ;

metaMapOptions : MetaMapOption (',' MetaMapOption)* ;


// aas

aas : 'aas([' (aasItem (',' aasItem)*)? ']).' ;

aasItem : AASAcronymExpansion '*[' numList? ']*[' strList? ']'  # aasItemNormal
        | AASAcronymExpansion2 '*[' strList? ']'                # aasItem2
        | AASAcronymExpansion3 '*[' strList? ']'                # aasItem3
        ;


// negation list

negList : 'neg_list([' (negation (',' negation)*)? ']).' ;

negation : 'negation(' Word ',' str ',[' positionList '],[' negatedConceptList '],[' positionList '])' ;

negatedConceptList : NegatedConcept (',' NegatedConcept)* ;


// utterance

utterance : 'utterance(' str ',' DQStr ',' positionList ',[' numList? ']).' ;


// phrase

phrase : 'phrase(' str ',[' taggingInfoList ']' ',' positionList ',[' numList? ']).' ;

taggingInfoList : taggingInfo (',' taggingInfo)* ;

taggingInfo : Word '([' tagProperty (',' tagProperty)* '])' ;

tagProperty : 'lexmatch([' str '])'               # phraseLexMatch
            | 'inputmatch([' strList '])'         # phraseInputMatch
            | 'tag(' Word ')'                     # phraseTag
            | 'tokens([' strList? '])'            # phraseTokens
            | 'features([' strList '])'           # phraseFeatures
            ;


// candidates

candidates : 'candidates(' N ',' N ',' N ',' N ',[' (ev (',' ev)*)? ']).' ;

ev : 'ev(-' N ',' str ',' str ',' str ',[' strList '],[' strList
     '],[[[' matchMap ('],[[' matchMap)* ']],' Word ',' Word ',[' strList '],[' positionList ']' ',' N (',' N)? ')' ;

matchMap : N ',' N '],[' N ',' N ']' ',' N ;


// mappings

mappings : 'mappings([' (map (',' map)*)? ']).' ;

map : 'map(-' N ',[' ev (',' ev)* '])' ;


// misc

numList : N (',' N)* ;

strList : str (',' str)* ;

str : Str
    | Word
    ;

positionList : Position (',' Position)* ;


///////////////////////////////////////////////////////////////////////////////////////
// lexer stuff

MetaMapOption : [a-z]+ ('_' [a-z]+)* '-' (('[' (Str|Word|N)? ']')|(Str|Word|N)) ;

AASAcronymExpansion : DQStr '*' DQStr ;
AASAcronymExpansion2 : DQStr '*' DQStr '*' DQStr ;
AASAcronymExpansion3 : (Str|Word) '*' (Str|Word) '*(' N ',' N ',' N ',' N ',' N ':' N ')' ;

Str : '\'' STR_CHR* '\'' ;

fragment STR_CHR : ~['\\]
                 | '\\' ['\\]
                 ;

DQStr : '"' ('""'|~["])* '"' ;

Word : LEADING_WORD_CHAR WORD_CHAR* ;

fragment LEADING_WORD_CHAR : '\\'
                           | ~[0-9,'"[\]() \t\r\n]
                           ;

fragment WORD_CHAR : '\\'
                   | ~[,'"[\]() \t\r\n]
                   ;

Position : N '/' N ;

NegatedConcept : (Str|Word) ':' (Str|Word) ;

N : [0-9]
  | [1-9] [0-9]+
  ;

ERROR_LINE : '[' N+ ']: *** ERROR *** ERROR *** ERROR ***' -> skip;

WS : [ \t\r\n]+ -> skip;
