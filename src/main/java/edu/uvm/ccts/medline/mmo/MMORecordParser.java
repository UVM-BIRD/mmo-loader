/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MMO Loader.
 *
 * MMO Loader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MMO Loader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MMO Loader.  If not, see <http:www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.medline.mmo;

import edu.uvm.ccts.common.db.parser.AbstractCustomFileParser;
import edu.uvm.ccts.common.db.parser.IdGenerator;
import edu.uvm.ccts.common.db.parser.MapTableIdRegistry;
import edu.uvm.ccts.common.db.parser.TableData;
import edu.uvm.ccts.common.util.HashUtil;
import edu.uvm.ccts.medline.mmo.antlr.MMOLexer;
import edu.uvm.ccts.medline.mmo.antlr.MMOParser;
import edu.uvm.ccts.medline.mmo.model.*;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;
import java.util.zip.GZIPInputStream;

/**
 * Created by mstorer on 5/8/14.
 */
public class MMORecordParser extends AbstractCustomFileParser {
    public static final String TABLE_FILE_RECORD = "record.txt";
    public static final String TABLE_FILE_UTTERANCE = "utterance.txt";
    public static final String TABLE_FILE_UTTERANCE_POS = "utterance_pos.txt";
    public static final String TABLE_FILE_PHRASE = "phrase.txt";
    public static final String TABLE_FILE_PHRASE_TAG = "phrase_tag.txt";
    public static final String TABLE_FILE_TAG = "tag.txt";
    public static final String TABLE_FILE_PHRASE_POS = "phrase_pos.txt";
    public static final String TABLE_FILE_MAPPING = "mapping.txt";
    public static final String TABLE_FILE_EV = "ev.txt";
    public static final String TABLE_FILE_EV_MATCHED_WORD = "ev_matched_word.txt";
    public static final String TABLE_FILE_EV_SEMANTIC_TYPE = "ev_semantic_type.txt";
    public static final String TABLE_FILE_EV_MATCH_MAP = "ev_match_map.txt";
    public static final String TABLE_FILE_EV_SOURCE = "ev_source.txt";
    public static final String TABLE_FILE_EV_POS = "ev_pos.txt";
    public static final String TABLE_FILE_NEGATION = "negation.txt";
    public static final String TABLE_FILE_NEG_TRIGGER_POS = "neg_trigger_pos.txt";
    public static final String TABLE_FILE_NEG_CONCEPT = "neg_concept.txt";
    public static final String TABLE_FILE_NEG_CONCEPT_POS = "neg_concept_pos.txt";

    private TableData tRecord;
    private TableData tUtterance;
    private TableData tUtterancePos;
    private TableData tPhrase;
    private TableData tPhraseTag;
    private TableData tTag;
    private TableData tPhrasePos;
    private TableData tMapping;
    private TableData tEv;
    private TableData tEvMatchedWord;
    private TableData tEvSemanticType;
    private TableData tEvMatchMap;
    private TableData tEvSource;
    private TableData tEvPos;
    private TableData tNegation;
    private TableData tNegTriggerPos;
    private TableData tNegConcept;
    private TableData tNegConceptPos;

    private List<TableData> tableDataList;

    private IdGenerator idGen;
    private MapTableIdRegistry mapTableIdReg;


    public MMORecordParser(int threadId, String tempDir, String outputDir) throws IOException {
        super(threadId);

        tRecord =           new TableData(tempDir, outputDir, TABLE_FILE_RECORD);
        tUtterance =        new TableData(tempDir, outputDir, TABLE_FILE_UTTERANCE);
        tUtterancePos =     new TableData(tempDir, outputDir, TABLE_FILE_UTTERANCE_POS);
        tPhrase =           new TableData(tempDir, outputDir, TABLE_FILE_PHRASE);
        tPhraseTag =        new TableData(tempDir, outputDir, TABLE_FILE_PHRASE_TAG);
        tTag =              new TableData(tempDir, outputDir, TABLE_FILE_TAG);
        tPhrasePos =        new TableData(tempDir, outputDir, TABLE_FILE_PHRASE_POS);
        tMapping =          new TableData(tempDir, outputDir, TABLE_FILE_MAPPING);
        tEv =               new TableData(tempDir, outputDir, TABLE_FILE_EV);
        tEvMatchedWord =    new TableData(tempDir, outputDir, TABLE_FILE_EV_MATCHED_WORD);
        tEvSemanticType =   new TableData(tempDir, outputDir, TABLE_FILE_EV_SEMANTIC_TYPE);
        tEvMatchMap =       new TableData(tempDir, outputDir, TABLE_FILE_EV_MATCH_MAP);
        tEvSource =         new TableData(tempDir, outputDir, TABLE_FILE_EV_SOURCE);
        tEvPos =            new TableData(tempDir, outputDir, TABLE_FILE_EV_POS);
        tNegation =         new TableData(tempDir, outputDir, TABLE_FILE_NEGATION);
        tNegTriggerPos =    new TableData(tempDir, outputDir, TABLE_FILE_NEG_TRIGGER_POS);
        tNegConcept =       new TableData(tempDir, outputDir, TABLE_FILE_NEG_CONCEPT);
        tNegConceptPos =    new TableData(tempDir, outputDir, TABLE_FILE_NEG_CONCEPT_POS);

        tableDataList = Arrays.asList(tRecord, tUtterance, tUtterancePos, tPhrase, tPhraseTag, tTag, tPhrasePos,
                tMapping, tEv, tEvMatchedWord, tEvSemanticType, tEvMatchMap, tEvSource, tEvPos, tNegation,
                tNegTriggerPos, tNegConcept, tNegConceptPos);

        idGen = IdGenerator.getInstance();
        mapTableIdReg = MapTableIdRegistry.getInstance();
    }


    @Override
    protected List<TableData> getTableDataList() {
        return tableDataList;
    }

    @Override
    protected InputStream getInputStream(String filename) throws IOException {
        InputStream in = new FileInputStream(filename);

        return filename.toLowerCase().endsWith(".gz") ?
                new GZIPInputStream(in) :
                in;
    }

    @Override
    protected String getRecordStartText() {
        return "args(";
    }

    @Override
    protected void processRecord(String s) throws Exception {
        MMOEvalVisitor visitor = new MMOEvalVisitor();
        visitor.visit(buildParseTree(s));
        updateTables(visitor.getRecord());
    }


    /**
     * Updates table-data buffers with information from the current record
     * @param r a {@link Record}
     * @throws IOException
     */
    private void updateTables(Record r) throws IOException, NoSuchAlgorithmException {
        int pk = r.getPartitionKey();

        int recordId = idGen.nextId(tRecord.getFilename());
        tRecord.addRecord(recordId, r.getPmid(), r.getType());

        for (Utterance u : r.getUtteranceList()) {
            int utteranceId = idGen.nextId(tUtterance.getFilename());
            tUtterance.addRecord(utteranceId, recordId, u.getSectionId(), u.getUtterance());

            for (Position pos : u.getPositionInfoList()) {
                tUtterancePos.addRecord(utteranceId, pos.getStartPos(), pos.getSpanLength());
            }

            for (Phrase p : u.getPhraseList()) {
                int phraseId = idGen.nextId(tPhrase.getFilename());
                tPhrase.addRecord(pk, phraseId, utteranceId, p.getIdentifiedNounPhrase());

                for (TaggingInfo ti : p.getTaggingInfoList()) {
                    String key = HashUtil.buildKey(ti.getFunction(), ti.getLexMatch(), ti.getInputMatch(),
                            ti.getTag(), ti.getTokens(), ti.getFeatures());

                    int tagId;
                    if (mapTableIdReg.hasId(tTag.getFilename(), key)) {
                        tagId = mapTableIdReg.getId(tTag.getFilename(), key);

                    } else {
                        tagId = mapTableIdReg.generateId(tTag.getFilename(), key);
                        tTag.addRecord(tagId, ti.getFunction(), ti.getLexMatch(), ti.getInputMatch(), ti.getTag(),
                                ti.getTokens(), ti.getFeatures());
                    }

                    tPhraseTag.addRecord(pk, phraseId, tagId);
                }

                for (Position pos : p.getPositionInfoList()) {
                    tPhrasePos.addRecord(pk, phraseId, pos.getStartPos(), pos.getSpanLength());
                }

                for (MapInfo mi : p.getMappings().getMapInfoList()) {
                    int mappingId = idGen.nextId(tMapping.getFilename());
                    tMapping.addRecord(pk, mappingId, phraseId, mi.getScore());

                    for (EV ev : mi.getEvList()) {
                        int evId = idGen.nextId(tEv.getFilename());
                        tEv.addRecord(pk, evId, mappingId, ev.getScore(), ev.getUmlsId(), ev.getConcept(),
                                ev.getConceptPrefName(), ev.getInvolvedWithHead(), ev.getOvermatch());

                        for (String matchedWord : ev.getMatchedWordList()) {
                            tEvMatchedWord.addRecord(pk, evId, matchedWord);
                        }

                        for (String semanticType : ev.getSemanticTypeList()) {
                            tEvSemanticType.addRecord(pk, evId, semanticType);
                        }

                        for (MatchMap mm : ev.getMatchMapList()) {
                            tEvMatchMap.addRecord(pk, evId, mm.getPhraseWordSpanBegin(), mm.getPhraseWordSpanEnd(),
                                    mm.getConceptWordSpanBegin(), mm.getConceptWordSpanEnd(), mm.getVariation());
                        }

                        for (String source : ev.getSourceList()) {
                            tEvSource.addRecord(pk, evId, source);
                        }

                        for (Position pos : ev.getPositionList()) {
                            tEvPos.addRecord(pk, evId, pos.getStartPos(), pos.getSpanLength());
                        }
                    }
                }
            }
        }

        for (Negation n : r.getNegList().getNegationList()) {
            int negationId = idGen.nextId(tNegation.getFilename());
            tNegation.addRecord(pk, negationId, recordId, n.getType(), n.getTrigger());

            for (Position pos : n.getTriggerPositionList()) {
                tNegTriggerPos.addRecord(pk, negationId, pos.getStartPos(), pos.getSpanLength());
            }

            for (NegatedConcept nc : n.getNegatedConceptList()) {
                tNegConcept.addRecord(pk, negationId, nc.getKey(), nc.getValue());
            }

            for (Position pos : n.getConceptPositionList()) {
                tNegConceptPos.addRecord(pk, negationId, pos.getStartPos(), pos.getSpanLength());
            }
        }
    }

    private ParseTree buildParseTree(String record) throws IOException {
        InputStream inputStream = new ByteArrayInputStream(record.getBytes());
        ANTLRInputStream input = new ANTLRInputStream(inputStream);
        MMOLexer lexer = new BailMMOLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MMOParser parser = new BailMMOParser(tokens);
        return parser.init();
    }
}
