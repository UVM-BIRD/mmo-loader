/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MMO Loader.
 *
 * MMO Loader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MMO Loader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MMO Loader.  If not, see <http:www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.medline.mmo.model;

import java.util.List;

/**
 * Created by mstorer on 5/7/14.
 */
public class EV {
    private Integer score;
    private String umlsId;
    private String concept;
    private String conceptPrefName;
    private List<String> matchedWordList;
    private List<String> semanticTypeList;
    private List<MatchMap> matchMapList;
    private Boolean involvedWithHead;
    private Boolean overmatch;
    private List<String> sourceList;
    private List<Position> positionList;
    private Integer n1;

    public EV(Integer score, String umlsId, String concept, String conceptPrefName, List<String> matchedWordList,
              List<String> semanticTypeList, List<MatchMap> matchMapList, Boolean involvedWithHead, Boolean overmatch,
              List<String> sourceList, List<Position> positionList, Integer n1) {

        this.score = score;
        this.umlsId = umlsId;
        this.concept = concept;
        this.conceptPrefName = conceptPrefName;
        this.matchedWordList = matchedWordList;
        this.semanticTypeList = semanticTypeList;
        this.matchMapList = matchMapList;
        this.involvedWithHead = involvedWithHead;
        this.overmatch = overmatch;
        this.sourceList = sourceList;
        this.positionList = positionList;
        this.n1 = n1;
    }

    public Integer getScore() {
        return score;
    }

    public String getUmlsId() {
        return umlsId;
    }

    public String getConcept() {
        return concept;
    }

    public String getConceptPrefName() {
        return conceptPrefName;
    }

    public List<String> getMatchedWordList() {
        return matchedWordList;
    }

    public List<String> getSemanticTypeList() {
        return semanticTypeList;
    }

    public List<MatchMap> getMatchMapList() {
        return matchMapList;
    }

    public Boolean getInvolvedWithHead() {
        return involvedWithHead;
    }

    public Boolean getOvermatch() {
        return overmatch;
    }

    public List<String> getSourceList() {
        return sourceList;
    }

    public List<Position> getPositionList() {
        return positionList;
    }

    public Integer getN1() {
        return n1;
    }
}
