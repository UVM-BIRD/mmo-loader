/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MMO Loader.
 *
 * MMO Loader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MMO Loader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MMO Loader.  If not, see <http:www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.medline.mmo;

import edu.uvm.ccts.common.db.loader.AbstractFileLoader;
import edu.uvm.ccts.common.db.parser.AbstractFileParser;
import edu.uvm.ccts.common.util.FileUtil;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by mstorer on 5/7/14.
 */
public class MMOLoader extends AbstractFileLoader {
    private static final String outputDir = "./out";

    private static final Map<String, String> tableFileMap = new LinkedHashMap<String, String>();
    static {
        tableFileMap.put("record",          MMORecordParser.TABLE_FILE_RECORD);
        tableFileMap.put("utterance",       MMORecordParser.TABLE_FILE_UTTERANCE);
        tableFileMap.put("utterancePos",    MMORecordParser.TABLE_FILE_UTTERANCE_POS);
        tableFileMap.put("phrase",          MMORecordParser.TABLE_FILE_PHRASE);
        tableFileMap.put("phrasePos",       MMORecordParser.TABLE_FILE_PHRASE_POS);
        tableFileMap.put("tag",             MMORecordParser.TABLE_FILE_TAG);
        tableFileMap.put("phraseTag",       MMORecordParser.TABLE_FILE_PHRASE_TAG);
        tableFileMap.put("mapping",         MMORecordParser.TABLE_FILE_MAPPING);
        tableFileMap.put("ev",              MMORecordParser.TABLE_FILE_EV);
        tableFileMap.put("evMatchedWord",   MMORecordParser.TABLE_FILE_EV_MATCHED_WORD);
        tableFileMap.put("evSemanticType",  MMORecordParser.TABLE_FILE_EV_SEMANTIC_TYPE);
        tableFileMap.put("evSource",        MMORecordParser.TABLE_FILE_EV_SOURCE);
        tableFileMap.put("evMatchMap",      MMORecordParser.TABLE_FILE_EV_MATCH_MAP);
        tableFileMap.put("evPos",           MMORecordParser.TABLE_FILE_EV_POS);
        tableFileMap.put("negation",        MMORecordParser.TABLE_FILE_NEGATION);
        tableFileMap.put("negTriggerPos",   MMORecordParser.TABLE_FILE_NEG_TRIGGER_POS);
        tableFileMap.put("negConcept",      MMORecordParser.TABLE_FILE_NEG_CONCEPT);
        tableFileMap.put("negConceptPos",   MMORecordParser.TABLE_FILE_NEG_CONCEPT_POS);
    }

    public MMOLoader() {
        super(outputDir);
    }

    @Override
    protected String getName() {
        return "mmo";
    }

    @Override
    protected int getRequiredMemPerThreadMB() {
        return 500;
    }

    @Override
    protected Map<String, String> getTableFileMap() {
        return tableFileMap;
    }

    @Override
    protected AbstractFileParser buildParser(int threadId, String tempDir) throws IOException {
        return new MMORecordParser(threadId, tempDir, outputDir);
    }

    @Override
    protected String getFilenameFilter() {
        return "*.gz";
    }

    @Override
    public void prepare(String path) throws InterruptedException, IOException {

        // since generated files make use of keys and mappings that are stored in memory,
        // and restarting a process halfway through will reset those counters and mappings,
        // the only safe solution is to start fresh each time.
        FileUtil.removeDirectory(outputDir);
        FileUtil.createDirectory(outputDir);

        super.prepare(path);
    }
}
