/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MMO Loader.
 *
 * MMO Loader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MMO Loader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MMO Loader.  If not, see <http:www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.medline.mmo.model;

/**
 * Created by mstorer on 5/7/14.
 */
public class MatchMap {
    private Integer phraseWordSpanBegin;
    private Integer phraseWordSpanEnd;
    private Integer conceptWordSpanBegin;
    private Integer conceptWordSpanEnd;
    private Integer variation;

    public MatchMap(Integer phraseWordSpanBegin, Integer phraseWordSpanEnd, Integer conceptWordSpanBegin,
                    Integer conceptWordSpanEnd, Integer variation) {

        this.phraseWordSpanBegin = phraseWordSpanBegin;
        this.phraseWordSpanEnd = phraseWordSpanEnd;
        this.conceptWordSpanBegin = conceptWordSpanBegin;
        this.conceptWordSpanEnd = conceptWordSpanEnd;
        this.variation = variation;
    }

    public Integer getPhraseWordSpanBegin() {
        return phraseWordSpanBegin;
    }

    public Integer getPhraseWordSpanEnd() {
        return phraseWordSpanEnd;
    }

    public Integer getConceptWordSpanBegin() {
        return conceptWordSpanBegin;
    }

    public Integer getConceptWordSpanEnd() {
        return conceptWordSpanEnd;
    }

    public Integer getVariation() {
        return variation;
    }
}
