/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MMO Loader.
 *
 * MMO Loader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MMO Loader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MMO Loader.  If not, see <http:www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.medline.mmo.model;

import java.util.List;

/**
 * Created by mstorer on 5/7/14.
 */
public class AasItem {
    private String acronym;
    private String expansion;
    private List<Integer> countList;
    private List<String> cuiList;

    public AasItem(String acronym, String expansion) {
        this.acronym = acronym;
        this.expansion = expansion;
    }

    public String getAcronym() {
        return acronym;
    }

    public String getExpansion() {
        return expansion;
    }

    public List<Integer> getCountList() {
        return countList;
    }

    public void setCountList(List<Integer> countList) {
        this.countList = countList;
    }

    public List<String> getCuiList() {
        return cuiList;
    }

    public void setCuiList(List<String> cuiList) {
        this.cuiList = cuiList;
    }
}
