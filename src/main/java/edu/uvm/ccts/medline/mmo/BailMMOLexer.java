/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MMO Loader.
 *
 * MMO Loader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MMO Loader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MMO Loader.  If not, see <http:www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.medline.mmo;

import edu.uvm.ccts.common.exceptions.antlr.AntlrException;
import edu.uvm.ccts.medline.mmo.antlr.MMOLexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.LexerNoViableAltException;

/**
 * Created by mstorer on 5/8/14.
 */
public class BailMMOLexer extends MMOLexer {
    public BailMMOLexer(CharStream input) {
        super(input);
    }

    @Override
    public void recover(LexerNoViableAltException e) {
        String logic = e.getInputStream().toString();
        String near = logic.substring(Math.max(0, e.getStartIndex() - 5),
                Math.min(e.getStartIndex() + 5, logic.length() - 1));

        String message = "failed to lex MMO record at index " + e.getStartIndex() + " near \"" + near + "\"";

        throw new AntlrException(AntlrException.Type.LEXER, message, e);
    }
}
