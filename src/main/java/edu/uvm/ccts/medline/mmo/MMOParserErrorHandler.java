/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MMO Loader.
 *
 * MMO Loader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MMO Loader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MMO Loader.  If not, see <http:www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.medline.mmo;

import edu.uvm.ccts.common.exceptions.antlr.AntlrException;
import org.antlr.v4.runtime.*;

/**
 * Created by mstorer on 5/8/14.
 */
public class MMOParserErrorHandler extends DefaultErrorStrategy {
    private boolean shouldReportErrors;

    public MMOParserErrorHandler(boolean shouldReportErrors) {
        this.shouldReportErrors = shouldReportErrors;
    }

    @Override
    public void recover(Parser recognizer, RecognitionException e) {
        String message = "failed to parse MMO record.  offending token: " + e.getOffendingToken().toString();
        throw new AntlrException(AntlrException.Type.PARSER, message, e);
    }

    @Override
    public Token recoverInline(Parser recognizer) throws RecognitionException {
        throw new AntlrException(AntlrException.Type.PARSER, new InputMismatchException(recognizer));
    }

    @Override
    public void reportError(Parser recognizer, RecognitionException e) {
        if (shouldReportErrors) {
            super.reportError(recognizer, e);
        }
    }

    @Override
    public void sync(Parser recognizer) { }
}
