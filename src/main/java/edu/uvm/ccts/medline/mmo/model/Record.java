/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MMO Loader.
 *
 * MMO Loader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MMO Loader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MMO Loader.  If not, see <http:www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.medline.mmo.model;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by mstorer on 5/7/14.
 */
public class Record {
    private int partitionKey;
    private int pmid;
    private String type;

    private Args args;
    private Aas aas;
    private NegList negList;
    private List<Utterance> utteranceList;

    public Record(Args args, Aas aas, NegList negList, List<Utterance> utteranceList) throws NoSuchAlgorithmException {
        this.args = args;
        this.aas = aas;
        this.negList = negList;
        this.utteranceList = utteranceList;

        String[] parts = utteranceList.get(0).getId().split("\\.");        // format : "16691646.ti.1"

        int wsIndex = parts[0].indexOf(" ");
        pmid = wsIndex >= 0 ?
                Integer.parseInt(parts[0].substring(0, wsIndex)) :         // for case when, e.g. : "22485199 [2]"
                Integer.parseInt(parts[0]);

        type = parts[1];

        buildPartitionKey(pmid);
    }

    public int getPartitionKey() {
        return partitionKey;
    }

    public int getPmid() {
        return pmid;
    }

    public String getType() {
        return type;
    }

    public Args getArgs() {
        return args;
    }

    public Aas getAas() {
        return aas;
    }

    public NegList getNegList() {
        return negList;
    }

    public List<Utterance> getUtteranceList() {
        return utteranceList;
    }

    private void buildPartitionKey(Object obj) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(String.valueOf(obj).getBytes());
        byte[] bytes = md.digest();
        ByteBuffer buf = ByteBuffer.wrap(Arrays.copyOfRange(bytes, bytes.length - 4, bytes.length));
        partitionKey = Math.abs(buf.getInt(0)) % 256;
    }
}
