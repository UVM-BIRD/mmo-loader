/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MMO Loader.
 *
 * MMO Loader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MMO Loader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MMO Loader.  If not, see <http:www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.medline.mmo.model;

/**
 * Created by mstorer on 5/7/14.
 */
public class TaggingInfo {
    private String function;
    private String lexMatch;
    private String inputMatch;
    private String tag;
    private String tokens;
    private String features;

    public TaggingInfo(String function, String lexMatch, String inputMatch, String tag, String tokens, String features) {
        this.function = function;
        this.lexMatch = lexMatch;
        this.inputMatch = inputMatch;
        this.tag = tag;
        this.tokens = tokens;
        this.features = features;
    }

    public String getFunction() {
        return function;
    }

    public String getLexMatch() {
        return lexMatch;
    }

    public String getInputMatch() {
        return inputMatch;
    }

    public String getTag() {
        return tag;
    }

    public String getTokens() {
        return tokens;
    }

    public String getFeatures() {
        return features;
    }
}