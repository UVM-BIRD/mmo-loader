/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MMO Loader.
 *
 * MMO Loader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MMO Loader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MMO Loader.  If not, see <http:www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.medline.mmo.model;

import java.util.List;

/**
 * Created by mstorer on 5/7/14.
 */
public class Phrase {
    private String identifiedNounPhrase;
    private List<TaggingInfo> taggingInfoList;
    private List<Position> positionInfoList;
    private List<Integer> nList;

    private Candidates candidates = null;
    private Mappings mappings = null;

    public Phrase(String identifiedNounPhrase, List<TaggingInfo> taggingInfoList, List<Position> positionInfoList,
                  List<Integer> nList) {

        this.identifiedNounPhrase = identifiedNounPhrase;
        this.taggingInfoList = taggingInfoList;
        this.positionInfoList = positionInfoList;
        this.nList = nList;
    }

    public String getIdentifiedNounPhrase() {
        return identifiedNounPhrase;
    }

    public List<TaggingInfo> getTaggingInfoList() {
        return taggingInfoList;
    }

    public List<Position> getPositionInfoList() {
        return positionInfoList;
    }

    public List<Integer> getnList() {
        return nList;
    }

    public Candidates getCandidates() {
        return candidates;
    }

    public void setCandidates(Candidates candidates) {
        this.candidates = candidates;
    }

    public Mappings getMappings() {
        return mappings;
    }

    public void setMappings(Mappings mappings) {
        this.mappings = mappings;
    }
}
