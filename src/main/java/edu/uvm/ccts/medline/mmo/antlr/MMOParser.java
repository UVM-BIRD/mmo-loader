// Generated from MMO.g4 by ANTLR 4.1
package edu.uvm.ccts.medline.mmo.antlr;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MMOParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__28=1, T__27=2, T__26=3, T__25=4, T__24=5, T__23=6, T__22=7, T__21=8, 
		T__20=9, T__19=10, T__18=11, T__17=12, T__16=13, T__15=14, T__14=15, T__13=16, 
		T__12=17, T__11=18, T__10=19, T__9=20, T__8=21, T__7=22, T__6=23, T__5=24, 
		T__4=25, T__3=26, T__2=27, T__1=28, T__0=29, MetaMapOption=30, AASAcronymExpansion=31, 
		AASAcronymExpansion2=32, AASAcronymExpansion3=33, Str=34, DQStr=35, Word=36, 
		Position=37, NegatedConcept=38, N=39, ERROR_LINE=40, WS=41;
	public static final String[] tokenNames = {
		"<INVALID>", "']'", "']*['", "'features(['", "','", "'(['", "']).'", "''EOU'.'", 
		"'])'", "'candidates('", "'mappings(['", "',['", "'phrase('", "')'", "'lexmatch(['", 
		"'neg_list(['", "'],['", "'],[['", "'ev(-'", "'aas(['", "'inputmatch(['", 
		"'tokens(['", "'*['", "'],[[['", "']],'", "'utterance('", "'args('", "'map(-'", 
		"'negation('", "'tag('", "MetaMapOption", "AASAcronymExpansion", "AASAcronymExpansion2", 
		"AASAcronymExpansion3", "Str", "DQStr", "Word", "Position", "NegatedConcept", 
		"N", "ERROR_LINE", "WS"
	};
	public static final int
		RULE_init = 0, RULE_upcm = 1, RULE_pcm = 2, RULE_args = 3, RULE_metaMapOptions = 4, 
		RULE_aas = 5, RULE_aasItem = 6, RULE_negList = 7, RULE_negation = 8, RULE_negatedConceptList = 9, 
		RULE_utterance = 10, RULE_phrase = 11, RULE_taggingInfoList = 12, RULE_taggingInfo = 13, 
		RULE_tagProperty = 14, RULE_candidates = 15, RULE_ev = 16, RULE_matchMap = 17, 
		RULE_mappings = 18, RULE_map = 19, RULE_numList = 20, RULE_strList = 21, 
		RULE_str = 22, RULE_positionList = 23;
	public static final String[] ruleNames = {
		"init", "upcm", "pcm", "args", "metaMapOptions", "aas", "aasItem", "negList", 
		"negation", "negatedConceptList", "utterance", "phrase", "taggingInfoList", 
		"taggingInfo", "tagProperty", "candidates", "ev", "matchMap", "mappings", 
		"map", "numList", "strList", "str", "positionList"
	};

	@Override
	public String getGrammarFileName() { return "MMO.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public MMOParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class InitContext extends ParserRuleContext {
		public AasContext aas() {
			return getRuleContext(AasContext.class,0);
		}
		public TerminalNode EOF() { return getToken(MMOParser.EOF, 0); }
		public NegListContext negList() {
			return getRuleContext(NegListContext.class,0);
		}
		public UpcmContext upcm(int i) {
			return getRuleContext(UpcmContext.class,i);
		}
		public List<UpcmContext> upcm() {
			return getRuleContexts(UpcmContext.class);
		}
		public ArgsContext args() {
			return getRuleContext(ArgsContext.class,0);
		}
		public InitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_init; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitInit(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InitContext init() throws RecognitionException {
		InitContext _localctx = new InitContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_init);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(48); args();
			setState(49); aas();
			setState(50); negList();
			setState(52); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(51); upcm();
				}
				}
				setState(54); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==25 );
			setState(56); match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UpcmContext extends ParserRuleContext {
		public List<PcmContext> pcm() {
			return getRuleContexts(PcmContext.class);
		}
		public UtteranceContext utterance() {
			return getRuleContext(UtteranceContext.class,0);
		}
		public PcmContext pcm(int i) {
			return getRuleContext(PcmContext.class,i);
		}
		public UpcmContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_upcm; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitUpcm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UpcmContext upcm() throws RecognitionException {
		UpcmContext _localctx = new UpcmContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_upcm);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(58); utterance();
			setState(60); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(59); pcm();
				}
				}
				setState(62); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==12 );
			setState(64); match(7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PcmContext extends ParserRuleContext {
		public PhraseContext phrase() {
			return getRuleContext(PhraseContext.class,0);
		}
		public CandidatesContext candidates() {
			return getRuleContext(CandidatesContext.class,0);
		}
		public MappingsContext mappings() {
			return getRuleContext(MappingsContext.class,0);
		}
		public PcmContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pcm; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitPcm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PcmContext pcm() throws RecognitionException {
		PcmContext _localctx = new PcmContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_pcm);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(66); phrase();
			setState(67); candidates();
			setState(68); mappings();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgsContext extends ParserRuleContext {
		public MetaMapOptionsContext metaMapOptions() {
			return getRuleContext(MetaMapOptionsContext.class,0);
		}
		public TerminalNode Str() { return getToken(MMOParser.Str, 0); }
		public ArgsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_args; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitArgs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgsContext args() throws RecognitionException {
		ArgsContext _localctx = new ArgsContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_args);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(70); match(26);
			setState(71); match(Str);
			setState(72); match(11);
			setState(73); metaMapOptions();
			setState(74); match(6);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MetaMapOptionsContext extends ParserRuleContext {
		public List<TerminalNode> MetaMapOption() { return getTokens(MMOParser.MetaMapOption); }
		public TerminalNode MetaMapOption(int i) {
			return getToken(MMOParser.MetaMapOption, i);
		}
		public MetaMapOptionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_metaMapOptions; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitMetaMapOptions(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MetaMapOptionsContext metaMapOptions() throws RecognitionException {
		MetaMapOptionsContext _localctx = new MetaMapOptionsContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_metaMapOptions);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(76); match(MetaMapOption);
			setState(81);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==4) {
				{
				{
				setState(77); match(4);
				setState(78); match(MetaMapOption);
				}
				}
				setState(83);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AasContext extends ParserRuleContext {
		public AasItemContext aasItem(int i) {
			return getRuleContext(AasItemContext.class,i);
		}
		public List<AasItemContext> aasItem() {
			return getRuleContexts(AasItemContext.class);
		}
		public AasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aas; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitAas(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AasContext aas() throws RecognitionException {
		AasContext _localctx = new AasContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_aas);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(84); match(19);
			setState(93);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << AASAcronymExpansion) | (1L << AASAcronymExpansion2) | (1L << AASAcronymExpansion3))) != 0)) {
				{
				setState(85); aasItem();
				setState(90);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==4) {
					{
					{
					setState(86); match(4);
					setState(87); aasItem();
					}
					}
					setState(92);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(95); match(6);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AasItemContext extends ParserRuleContext {
		public AasItemContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aasItem; }
	 
		public AasItemContext() { }
		public void copyFrom(AasItemContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AasItemNormalContext extends AasItemContext {
		public StrListContext strList() {
			return getRuleContext(StrListContext.class,0);
		}
		public NumListContext numList() {
			return getRuleContext(NumListContext.class,0);
		}
		public TerminalNode AASAcronymExpansion() { return getToken(MMOParser.AASAcronymExpansion, 0); }
		public AasItemNormalContext(AasItemContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitAasItemNormal(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AasItem2Context extends AasItemContext {
		public StrListContext strList() {
			return getRuleContext(StrListContext.class,0);
		}
		public TerminalNode AASAcronymExpansion2() { return getToken(MMOParser.AASAcronymExpansion2, 0); }
		public AasItem2Context(AasItemContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitAasItem2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AasItem3Context extends AasItemContext {
		public StrListContext strList() {
			return getRuleContext(StrListContext.class,0);
		}
		public TerminalNode AASAcronymExpansion3() { return getToken(MMOParser.AASAcronymExpansion3, 0); }
		public AasItem3Context(AasItemContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitAasItem3(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AasItemContext aasItem() throws RecognitionException {
		AasItemContext _localctx = new AasItemContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_aasItem);
		int _la;
		try {
			setState(119);
			switch (_input.LA(1)) {
			case AASAcronymExpansion:
				_localctx = new AasItemNormalContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(97); match(AASAcronymExpansion);
				setState(98); match(22);
				setState(100);
				_la = _input.LA(1);
				if (_la==N) {
					{
					setState(99); numList();
					}
				}

				setState(102); match(2);
				setState(104);
				_la = _input.LA(1);
				if (_la==Str || _la==Word) {
					{
					setState(103); strList();
					}
				}

				setState(106); match(1);
				}
				break;
			case AASAcronymExpansion2:
				_localctx = new AasItem2Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(107); match(AASAcronymExpansion2);
				setState(108); match(22);
				setState(110);
				_la = _input.LA(1);
				if (_la==Str || _la==Word) {
					{
					setState(109); strList();
					}
				}

				setState(112); match(1);
				}
				break;
			case AASAcronymExpansion3:
				_localctx = new AasItem3Context(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(113); match(AASAcronymExpansion3);
				setState(114); match(22);
				setState(116);
				_la = _input.LA(1);
				if (_la==Str || _la==Word) {
					{
					setState(115); strList();
					}
				}

				setState(118); match(1);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NegListContext extends ParserRuleContext {
		public NegationContext negation(int i) {
			return getRuleContext(NegationContext.class,i);
		}
		public List<NegationContext> negation() {
			return getRuleContexts(NegationContext.class);
		}
		public NegListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_negList; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitNegList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NegListContext negList() throws RecognitionException {
		NegListContext _localctx = new NegListContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_negList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(121); match(15);
			setState(130);
			_la = _input.LA(1);
			if (_la==28) {
				{
				setState(122); negation();
				setState(127);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==4) {
					{
					{
					setState(123); match(4);
					setState(124); negation();
					}
					}
					setState(129);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(132); match(6);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NegationContext extends ParserRuleContext {
		public StrContext str() {
			return getRuleContext(StrContext.class,0);
		}
		public TerminalNode Word() { return getToken(MMOParser.Word, 0); }
		public NegatedConceptListContext negatedConceptList() {
			return getRuleContext(NegatedConceptListContext.class,0);
		}
		public PositionListContext positionList(int i) {
			return getRuleContext(PositionListContext.class,i);
		}
		public List<PositionListContext> positionList() {
			return getRuleContexts(PositionListContext.class);
		}
		public NegationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_negation; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitNegation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NegationContext negation() throws RecognitionException {
		NegationContext _localctx = new NegationContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_negation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(134); match(28);
			setState(135); match(Word);
			setState(136); match(4);
			setState(137); str();
			setState(138); match(11);
			setState(139); positionList();
			setState(140); match(16);
			setState(141); negatedConceptList();
			setState(142); match(16);
			setState(143); positionList();
			setState(144); match(8);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NegatedConceptListContext extends ParserRuleContext {
		public TerminalNode NegatedConcept(int i) {
			return getToken(MMOParser.NegatedConcept, i);
		}
		public List<TerminalNode> NegatedConcept() { return getTokens(MMOParser.NegatedConcept); }
		public NegatedConceptListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_negatedConceptList; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitNegatedConceptList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NegatedConceptListContext negatedConceptList() throws RecognitionException {
		NegatedConceptListContext _localctx = new NegatedConceptListContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_negatedConceptList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(146); match(NegatedConcept);
			setState(151);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==4) {
				{
				{
				setState(147); match(4);
				setState(148); match(NegatedConcept);
				}
				}
				setState(153);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UtteranceContext extends ParserRuleContext {
		public TerminalNode DQStr() { return getToken(MMOParser.DQStr, 0); }
		public NumListContext numList() {
			return getRuleContext(NumListContext.class,0);
		}
		public StrContext str() {
			return getRuleContext(StrContext.class,0);
		}
		public PositionListContext positionList() {
			return getRuleContext(PositionListContext.class,0);
		}
		public UtteranceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_utterance; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitUtterance(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UtteranceContext utterance() throws RecognitionException {
		UtteranceContext _localctx = new UtteranceContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_utterance);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(154); match(25);
			setState(155); str();
			setState(156); match(4);
			setState(157); match(DQStr);
			setState(158); match(4);
			setState(159); positionList();
			setState(160); match(11);
			setState(162);
			_la = _input.LA(1);
			if (_la==N) {
				{
				setState(161); numList();
				}
			}

			setState(164); match(6);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PhraseContext extends ParserRuleContext {
		public TaggingInfoListContext taggingInfoList() {
			return getRuleContext(TaggingInfoListContext.class,0);
		}
		public NumListContext numList() {
			return getRuleContext(NumListContext.class,0);
		}
		public StrContext str() {
			return getRuleContext(StrContext.class,0);
		}
		public PositionListContext positionList() {
			return getRuleContext(PositionListContext.class,0);
		}
		public PhraseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_phrase; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitPhrase(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PhraseContext phrase() throws RecognitionException {
		PhraseContext _localctx = new PhraseContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_phrase);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(166); match(12);
			setState(167); str();
			setState(168); match(11);
			setState(169); taggingInfoList();
			setState(170); match(1);
			setState(171); match(4);
			setState(172); positionList();
			setState(173); match(11);
			setState(175);
			_la = _input.LA(1);
			if (_la==N) {
				{
				setState(174); numList();
				}
			}

			setState(177); match(6);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TaggingInfoListContext extends ParserRuleContext {
		public List<TaggingInfoContext> taggingInfo() {
			return getRuleContexts(TaggingInfoContext.class);
		}
		public TaggingInfoContext taggingInfo(int i) {
			return getRuleContext(TaggingInfoContext.class,i);
		}
		public TaggingInfoListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_taggingInfoList; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitTaggingInfoList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TaggingInfoListContext taggingInfoList() throws RecognitionException {
		TaggingInfoListContext _localctx = new TaggingInfoListContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_taggingInfoList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(179); taggingInfo();
			setState(184);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==4) {
				{
				{
				setState(180); match(4);
				setState(181); taggingInfo();
				}
				}
				setState(186);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TaggingInfoContext extends ParserRuleContext {
		public List<TagPropertyContext> tagProperty() {
			return getRuleContexts(TagPropertyContext.class);
		}
		public TerminalNode Word() { return getToken(MMOParser.Word, 0); }
		public TagPropertyContext tagProperty(int i) {
			return getRuleContext(TagPropertyContext.class,i);
		}
		public TaggingInfoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_taggingInfo; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitTaggingInfo(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TaggingInfoContext taggingInfo() throws RecognitionException {
		TaggingInfoContext _localctx = new TaggingInfoContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_taggingInfo);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(187); match(Word);
			setState(188); match(5);
			setState(189); tagProperty();
			setState(194);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==4) {
				{
				{
				setState(190); match(4);
				setState(191); tagProperty();
				}
				}
				setState(196);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(197); match(8);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TagPropertyContext extends ParserRuleContext {
		public TagPropertyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tagProperty; }
	 
		public TagPropertyContext() { }
		public void copyFrom(TagPropertyContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PhraseFeaturesContext extends TagPropertyContext {
		public StrListContext strList() {
			return getRuleContext(StrListContext.class,0);
		}
		public PhraseFeaturesContext(TagPropertyContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitPhraseFeatures(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PhraseLexMatchContext extends TagPropertyContext {
		public StrContext str() {
			return getRuleContext(StrContext.class,0);
		}
		public PhraseLexMatchContext(TagPropertyContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitPhraseLexMatch(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PhraseTokensContext extends TagPropertyContext {
		public StrListContext strList() {
			return getRuleContext(StrListContext.class,0);
		}
		public PhraseTokensContext(TagPropertyContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitPhraseTokens(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PhraseInputMatchContext extends TagPropertyContext {
		public StrListContext strList() {
			return getRuleContext(StrListContext.class,0);
		}
		public PhraseInputMatchContext(TagPropertyContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitPhraseInputMatch(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PhraseTagContext extends TagPropertyContext {
		public TerminalNode Word() { return getToken(MMOParser.Word, 0); }
		public PhraseTagContext(TagPropertyContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitPhraseTag(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TagPropertyContext tagProperty() throws RecognitionException {
		TagPropertyContext _localctx = new TagPropertyContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_tagProperty);
		int _la;
		try {
			setState(219);
			switch (_input.LA(1)) {
			case 14:
				_localctx = new PhraseLexMatchContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(199); match(14);
				setState(200); str();
				setState(201); match(8);
				}
				break;
			case 20:
				_localctx = new PhraseInputMatchContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(203); match(20);
				setState(204); strList();
				setState(205); match(8);
				}
				break;
			case 29:
				_localctx = new PhraseTagContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(207); match(29);
				setState(208); match(Word);
				setState(209); match(13);
				}
				break;
			case 21:
				_localctx = new PhraseTokensContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(210); match(21);
				setState(212);
				_la = _input.LA(1);
				if (_la==Str || _la==Word) {
					{
					setState(211); strList();
					}
				}

				setState(214); match(8);
				}
				break;
			case 3:
				_localctx = new PhraseFeaturesContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(215); match(3);
				setState(216); strList();
				setState(217); match(8);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CandidatesContext extends ParserRuleContext {
		public EvContext ev(int i) {
			return getRuleContext(EvContext.class,i);
		}
		public TerminalNode N(int i) {
			return getToken(MMOParser.N, i);
		}
		public List<EvContext> ev() {
			return getRuleContexts(EvContext.class);
		}
		public List<TerminalNode> N() { return getTokens(MMOParser.N); }
		public CandidatesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_candidates; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitCandidates(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CandidatesContext candidates() throws RecognitionException {
		CandidatesContext _localctx = new CandidatesContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_candidates);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(221); match(9);
			setState(222); match(N);
			setState(223); match(4);
			setState(224); match(N);
			setState(225); match(4);
			setState(226); match(N);
			setState(227); match(4);
			setState(228); match(N);
			setState(229); match(11);
			setState(238);
			_la = _input.LA(1);
			if (_la==18) {
				{
				setState(230); ev();
				setState(235);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==4) {
					{
					{
					setState(231); match(4);
					setState(232); ev();
					}
					}
					setState(237);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(240); match(6);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EvContext extends ParserRuleContext {
		public List<StrListContext> strList() {
			return getRuleContexts(StrListContext.class);
		}
		public StrContext str(int i) {
			return getRuleContext(StrContext.class,i);
		}
		public TerminalNode N(int i) {
			return getToken(MMOParser.N, i);
		}
		public List<StrContext> str() {
			return getRuleContexts(StrContext.class);
		}
		public TerminalNode Word(int i) {
			return getToken(MMOParser.Word, i);
		}
		public MatchMapContext matchMap(int i) {
			return getRuleContext(MatchMapContext.class,i);
		}
		public List<MatchMapContext> matchMap() {
			return getRuleContexts(MatchMapContext.class);
		}
		public List<TerminalNode> Word() { return getTokens(MMOParser.Word); }
		public List<TerminalNode> N() { return getTokens(MMOParser.N); }
		public PositionListContext positionList() {
			return getRuleContext(PositionListContext.class,0);
		}
		public StrListContext strList(int i) {
			return getRuleContext(StrListContext.class,i);
		}
		public EvContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ev; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitEv(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EvContext ev() throws RecognitionException {
		EvContext _localctx = new EvContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_ev);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(242); match(18);
			setState(243); match(N);
			setState(244); match(4);
			setState(245); str();
			setState(246); match(4);
			setState(247); str();
			setState(248); match(4);
			setState(249); str();
			setState(250); match(11);
			setState(251); strList();
			setState(252); match(16);
			setState(253); strList();
			setState(254); match(23);
			setState(255); matchMap();
			setState(260);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==17) {
				{
				{
				setState(256); match(17);
				setState(257); matchMap();
				}
				}
				setState(262);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(263); match(24);
			setState(264); match(Word);
			setState(265); match(4);
			setState(266); match(Word);
			setState(267); match(11);
			setState(268); strList();
			setState(269); match(16);
			setState(270); positionList();
			setState(271); match(1);
			setState(272); match(4);
			setState(273); match(N);
			setState(276);
			_la = _input.LA(1);
			if (_la==4) {
				{
				setState(274); match(4);
				setState(275); match(N);
				}
			}

			setState(278); match(13);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MatchMapContext extends ParserRuleContext {
		public TerminalNode N(int i) {
			return getToken(MMOParser.N, i);
		}
		public List<TerminalNode> N() { return getTokens(MMOParser.N); }
		public MatchMapContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_matchMap; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitMatchMap(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MatchMapContext matchMap() throws RecognitionException {
		MatchMapContext _localctx = new MatchMapContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_matchMap);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(280); match(N);
			setState(281); match(4);
			setState(282); match(N);
			setState(283); match(16);
			setState(284); match(N);
			setState(285); match(4);
			setState(286); match(N);
			setState(287); match(1);
			setState(288); match(4);
			setState(289); match(N);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MappingsContext extends ParserRuleContext {
		public List<MapContext> map() {
			return getRuleContexts(MapContext.class);
		}
		public MapContext map(int i) {
			return getRuleContext(MapContext.class,i);
		}
		public MappingsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mappings; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitMappings(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MappingsContext mappings() throws RecognitionException {
		MappingsContext _localctx = new MappingsContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_mappings);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(291); match(10);
			setState(300);
			_la = _input.LA(1);
			if (_la==27) {
				{
				setState(292); map();
				setState(297);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==4) {
					{
					{
					setState(293); match(4);
					setState(294); map();
					}
					}
					setState(299);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(302); match(6);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MapContext extends ParserRuleContext {
		public EvContext ev(int i) {
			return getRuleContext(EvContext.class,i);
		}
		public List<EvContext> ev() {
			return getRuleContexts(EvContext.class);
		}
		public TerminalNode N() { return getToken(MMOParser.N, 0); }
		public MapContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_map; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitMap(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MapContext map() throws RecognitionException {
		MapContext _localctx = new MapContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_map);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(304); match(27);
			setState(305); match(N);
			setState(306); match(11);
			setState(307); ev();
			setState(312);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==4) {
				{
				{
				setState(308); match(4);
				setState(309); ev();
				}
				}
				setState(314);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(315); match(8);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumListContext extends ParserRuleContext {
		public TerminalNode N(int i) {
			return getToken(MMOParser.N, i);
		}
		public List<TerminalNode> N() { return getTokens(MMOParser.N); }
		public NumListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numList; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitNumList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumListContext numList() throws RecognitionException {
		NumListContext _localctx = new NumListContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_numList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(317); match(N);
			setState(322);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==4) {
				{
				{
				setState(318); match(4);
				setState(319); match(N);
				}
				}
				setState(324);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StrListContext extends ParserRuleContext {
		public StrContext str(int i) {
			return getRuleContext(StrContext.class,i);
		}
		public List<StrContext> str() {
			return getRuleContexts(StrContext.class);
		}
		public StrListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_strList; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitStrList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StrListContext strList() throws RecognitionException {
		StrListContext _localctx = new StrListContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_strList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(325); str();
			setState(330);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==4) {
				{
				{
				setState(326); match(4);
				setState(327); str();
				}
				}
				setState(332);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StrContext extends ParserRuleContext {
		public TerminalNode Word() { return getToken(MMOParser.Word, 0); }
		public TerminalNode Str() { return getToken(MMOParser.Str, 0); }
		public StrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_str; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitStr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StrContext str() throws RecognitionException {
		StrContext _localctx = new StrContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_str);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(333);
			_la = _input.LA(1);
			if ( !(_la==Str || _la==Word) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PositionListContext extends ParserRuleContext {
		public TerminalNode Position(int i) {
			return getToken(MMOParser.Position, i);
		}
		public List<TerminalNode> Position() { return getTokens(MMOParser.Position); }
		public PositionListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_positionList; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MMOVisitor ) return ((MMOVisitor<? extends T>)visitor).visitPositionList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PositionListContext positionList() throws RecognitionException {
		PositionListContext _localctx = new PositionListContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_positionList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(335); match(Position);
			setState(340);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==4) {
				{
				{
				setState(336); match(4);
				setState(337); match(Position);
				}
				}
				setState(342);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\uacf5\uee8c\u4f5d\u8b0d\u4a45\u78bd\u1b2f\u3378\3+\u015a\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\3\2\3\2\3\2\3\2\6\2\67\n\2\r\2\16\28\3\2\3\2\3\3\3\3\6\3?\n\3\r\3\16"+
		"\3@\3\3\3\3\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\7\6R\n"+
		"\6\f\6\16\6U\13\6\3\7\3\7\3\7\3\7\7\7[\n\7\f\7\16\7^\13\7\5\7`\n\7\3\7"+
		"\3\7\3\b\3\b\3\b\5\bg\n\b\3\b\3\b\5\bk\n\b\3\b\3\b\3\b\3\b\5\bq\n\b\3"+
		"\b\3\b\3\b\3\b\5\bw\n\b\3\b\5\bz\n\b\3\t\3\t\3\t\3\t\7\t\u0080\n\t\f\t"+
		"\16\t\u0083\13\t\5\t\u0085\n\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n"+
		"\3\n\3\n\3\n\3\n\3\13\3\13\3\13\7\13\u0098\n\13\f\13\16\13\u009b\13\13"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u00a5\n\f\3\f\3\f\3\r\3\r\3\r\3\r"+
		"\3\r\3\r\3\r\3\r\3\r\5\r\u00b2\n\r\3\r\3\r\3\16\3\16\3\16\7\16\u00b9\n"+
		"\16\f\16\16\16\u00bc\13\16\3\17\3\17\3\17\3\17\3\17\7\17\u00c3\n\17\f"+
		"\17\16\17\u00c6\13\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\20\3\20\3\20\5\20\u00d7\n\20\3\20\3\20\3\20\3\20\3\20\5\20"+
		"\u00de\n\20\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21"+
		"\7\21\u00ec\n\21\f\21\16\21\u00ef\13\21\5\21\u00f1\n\21\3\21\3\21\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22"+
		"\3\22\7\22\u0105\n\22\f\22\16\22\u0108\13\22\3\22\3\22\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\5\22\u0117\n\22\3\22\3\22\3\23"+
		"\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24"+
		"\7\24\u012a\n\24\f\24\16\24\u012d\13\24\5\24\u012f\n\24\3\24\3\24\3\25"+
		"\3\25\3\25\3\25\3\25\3\25\7\25\u0139\n\25\f\25\16\25\u013c\13\25\3\25"+
		"\3\25\3\26\3\26\3\26\7\26\u0143\n\26\f\26\16\26\u0146\13\26\3\27\3\27"+
		"\3\27\7\27\u014b\n\27\f\27\16\27\u014e\13\27\3\30\3\30\3\31\3\31\3\31"+
		"\7\31\u0155\n\31\f\31\16\31\u0158\13\31\3\31\2\32\2\4\6\b\n\f\16\20\22"+
		"\24\26\30\32\34\36 \"$&(*,.\60\2\3\4\2$$&&\u0162\2\62\3\2\2\2\4<\3\2\2"+
		"\2\6D\3\2\2\2\bH\3\2\2\2\nN\3\2\2\2\fV\3\2\2\2\16y\3\2\2\2\20{\3\2\2\2"+
		"\22\u0088\3\2\2\2\24\u0094\3\2\2\2\26\u009c\3\2\2\2\30\u00a8\3\2\2\2\32"+
		"\u00b5\3\2\2\2\34\u00bd\3\2\2\2\36\u00dd\3\2\2\2 \u00df\3\2\2\2\"\u00f4"+
		"\3\2\2\2$\u011a\3\2\2\2&\u0125\3\2\2\2(\u0132\3\2\2\2*\u013f\3\2\2\2,"+
		"\u0147\3\2\2\2.\u014f\3\2\2\2\60\u0151\3\2\2\2\62\63\5\b\5\2\63\64\5\f"+
		"\7\2\64\66\5\20\t\2\65\67\5\4\3\2\66\65\3\2\2\2\678\3\2\2\28\66\3\2\2"+
		"\289\3\2\2\29:\3\2\2\2:;\7\2\2\3;\3\3\2\2\2<>\5\26\f\2=?\5\6\4\2>=\3\2"+
		"\2\2?@\3\2\2\2@>\3\2\2\2@A\3\2\2\2AB\3\2\2\2BC\7\t\2\2C\5\3\2\2\2DE\5"+
		"\30\r\2EF\5 \21\2FG\5&\24\2G\7\3\2\2\2HI\7\34\2\2IJ\7$\2\2JK\7\r\2\2K"+
		"L\5\n\6\2LM\7\b\2\2M\t\3\2\2\2NS\7 \2\2OP\7\6\2\2PR\7 \2\2QO\3\2\2\2R"+
		"U\3\2\2\2SQ\3\2\2\2ST\3\2\2\2T\13\3\2\2\2US\3\2\2\2V_\7\25\2\2W\\\5\16"+
		"\b\2XY\7\6\2\2Y[\5\16\b\2ZX\3\2\2\2[^\3\2\2\2\\Z\3\2\2\2\\]\3\2\2\2]`"+
		"\3\2\2\2^\\\3\2\2\2_W\3\2\2\2_`\3\2\2\2`a\3\2\2\2ab\7\b\2\2b\r\3\2\2\2"+
		"cd\7!\2\2df\7\30\2\2eg\5*\26\2fe\3\2\2\2fg\3\2\2\2gh\3\2\2\2hj\7\4\2\2"+
		"ik\5,\27\2ji\3\2\2\2jk\3\2\2\2kl\3\2\2\2lz\7\3\2\2mn\7\"\2\2np\7\30\2"+
		"\2oq\5,\27\2po\3\2\2\2pq\3\2\2\2qr\3\2\2\2rz\7\3\2\2st\7#\2\2tv\7\30\2"+
		"\2uw\5,\27\2vu\3\2\2\2vw\3\2\2\2wx\3\2\2\2xz\7\3\2\2yc\3\2\2\2ym\3\2\2"+
		"\2ys\3\2\2\2z\17\3\2\2\2{\u0084\7\21\2\2|\u0081\5\22\n\2}~\7\6\2\2~\u0080"+
		"\5\22\n\2\177}\3\2\2\2\u0080\u0083\3\2\2\2\u0081\177\3\2\2\2\u0081\u0082"+
		"\3\2\2\2\u0082\u0085\3\2\2\2\u0083\u0081\3\2\2\2\u0084|\3\2\2\2\u0084"+
		"\u0085\3\2\2\2\u0085\u0086\3\2\2\2\u0086\u0087\7\b\2\2\u0087\21\3\2\2"+
		"\2\u0088\u0089\7\36\2\2\u0089\u008a\7&\2\2\u008a\u008b\7\6\2\2\u008b\u008c"+
		"\5.\30\2\u008c\u008d\7\r\2\2\u008d\u008e\5\60\31\2\u008e\u008f\7\22\2"+
		"\2\u008f\u0090\5\24\13\2\u0090\u0091\7\22\2\2\u0091\u0092\5\60\31\2\u0092"+
		"\u0093\7\n\2\2\u0093\23\3\2\2\2\u0094\u0099\7(\2\2\u0095\u0096\7\6\2\2"+
		"\u0096\u0098\7(\2\2\u0097\u0095\3\2\2\2\u0098\u009b\3\2\2\2\u0099\u0097"+
		"\3\2\2\2\u0099\u009a\3\2\2\2\u009a\25\3\2\2\2\u009b\u0099\3\2\2\2\u009c"+
		"\u009d\7\33\2\2\u009d\u009e\5.\30\2\u009e\u009f\7\6\2\2\u009f\u00a0\7"+
		"%\2\2\u00a0\u00a1\7\6\2\2\u00a1\u00a2\5\60\31\2\u00a2\u00a4\7\r\2\2\u00a3"+
		"\u00a5\5*\26\2\u00a4\u00a3\3\2\2\2\u00a4\u00a5\3\2\2\2\u00a5\u00a6\3\2"+
		"\2\2\u00a6\u00a7\7\b\2\2\u00a7\27\3\2\2\2\u00a8\u00a9\7\16\2\2\u00a9\u00aa"+
		"\5.\30\2\u00aa\u00ab\7\r\2\2\u00ab\u00ac\5\32\16\2\u00ac\u00ad\7\3\2\2"+
		"\u00ad\u00ae\7\6\2\2\u00ae\u00af\5\60\31\2\u00af\u00b1\7\r\2\2\u00b0\u00b2"+
		"\5*\26\2\u00b1\u00b0\3\2\2\2\u00b1\u00b2\3\2\2\2\u00b2\u00b3\3\2\2\2\u00b3"+
		"\u00b4\7\b\2\2\u00b4\31\3\2\2\2\u00b5\u00ba\5\34\17\2\u00b6\u00b7\7\6"+
		"\2\2\u00b7\u00b9\5\34\17\2\u00b8\u00b6\3\2\2\2\u00b9\u00bc\3\2\2\2\u00ba"+
		"\u00b8\3\2\2\2\u00ba\u00bb\3\2\2\2\u00bb\33\3\2\2\2\u00bc\u00ba\3\2\2"+
		"\2\u00bd\u00be\7&\2\2\u00be\u00bf\7\7\2\2\u00bf\u00c4\5\36\20\2\u00c0"+
		"\u00c1\7\6\2\2\u00c1\u00c3\5\36\20\2\u00c2\u00c0\3\2\2\2\u00c3\u00c6\3"+
		"\2\2\2\u00c4\u00c2\3\2\2\2\u00c4\u00c5\3\2\2\2\u00c5\u00c7\3\2\2\2\u00c6"+
		"\u00c4\3\2\2\2\u00c7\u00c8\7\n\2\2\u00c8\35\3\2\2\2\u00c9\u00ca\7\20\2"+
		"\2\u00ca\u00cb\5.\30\2\u00cb\u00cc\7\n\2\2\u00cc\u00de\3\2\2\2\u00cd\u00ce"+
		"\7\26\2\2\u00ce\u00cf\5,\27\2\u00cf\u00d0\7\n\2\2\u00d0\u00de\3\2\2\2"+
		"\u00d1\u00d2\7\37\2\2\u00d2\u00d3\7&\2\2\u00d3\u00de\7\17\2\2\u00d4\u00d6"+
		"\7\27\2\2\u00d5\u00d7\5,\27\2\u00d6\u00d5\3\2\2\2\u00d6\u00d7\3\2\2\2"+
		"\u00d7\u00d8\3\2\2\2\u00d8\u00de\7\n\2\2\u00d9\u00da\7\5\2\2\u00da\u00db"+
		"\5,\27\2\u00db\u00dc\7\n\2\2\u00dc\u00de\3\2\2\2\u00dd\u00c9\3\2\2\2\u00dd"+
		"\u00cd\3\2\2\2\u00dd\u00d1\3\2\2\2\u00dd\u00d4\3\2\2\2\u00dd\u00d9\3\2"+
		"\2\2\u00de\37\3\2\2\2\u00df\u00e0\7\13\2\2\u00e0\u00e1\7)\2\2\u00e1\u00e2"+
		"\7\6\2\2\u00e2\u00e3\7)\2\2\u00e3\u00e4\7\6\2\2\u00e4\u00e5\7)\2\2\u00e5"+
		"\u00e6\7\6\2\2\u00e6\u00e7\7)\2\2\u00e7\u00f0\7\r\2\2\u00e8\u00ed\5\""+
		"\22\2\u00e9\u00ea\7\6\2\2\u00ea\u00ec\5\"\22\2\u00eb\u00e9\3\2\2\2\u00ec"+
		"\u00ef\3\2\2\2\u00ed\u00eb\3\2\2\2\u00ed\u00ee\3\2\2\2\u00ee\u00f1\3\2"+
		"\2\2\u00ef\u00ed\3\2\2\2\u00f0\u00e8\3\2\2\2\u00f0\u00f1\3\2\2\2\u00f1"+
		"\u00f2\3\2\2\2\u00f2\u00f3\7\b\2\2\u00f3!\3\2\2\2\u00f4\u00f5\7\24\2\2"+
		"\u00f5\u00f6\7)\2\2\u00f6\u00f7\7\6\2\2\u00f7\u00f8\5.\30\2\u00f8\u00f9"+
		"\7\6\2\2\u00f9\u00fa\5.\30\2\u00fa\u00fb\7\6\2\2\u00fb\u00fc\5.\30\2\u00fc"+
		"\u00fd\7\r\2\2\u00fd\u00fe\5,\27\2\u00fe\u00ff\7\22\2\2\u00ff\u0100\5"+
		",\27\2\u0100\u0101\7\31\2\2\u0101\u0106\5$\23\2\u0102\u0103\7\23\2\2\u0103"+
		"\u0105\5$\23\2\u0104\u0102\3\2\2\2\u0105\u0108\3\2\2\2\u0106\u0104\3\2"+
		"\2\2\u0106\u0107\3\2\2\2\u0107\u0109\3\2\2\2\u0108\u0106\3\2\2\2\u0109"+
		"\u010a\7\32\2\2\u010a\u010b\7&\2\2\u010b\u010c\7\6\2\2\u010c\u010d\7&"+
		"\2\2\u010d\u010e\7\r\2\2\u010e\u010f\5,\27\2\u010f\u0110\7\22\2\2\u0110"+
		"\u0111\5\60\31\2\u0111\u0112\7\3\2\2\u0112\u0113\7\6\2\2\u0113\u0116\7"+
		")\2\2\u0114\u0115\7\6\2\2\u0115\u0117\7)\2\2\u0116\u0114\3\2\2\2\u0116"+
		"\u0117\3\2\2\2\u0117\u0118\3\2\2\2\u0118\u0119\7\17\2\2\u0119#\3\2\2\2"+
		"\u011a\u011b\7)\2\2\u011b\u011c\7\6\2\2\u011c\u011d\7)\2\2\u011d\u011e"+
		"\7\22\2\2\u011e\u011f\7)\2\2\u011f\u0120\7\6\2\2\u0120\u0121\7)\2\2\u0121"+
		"\u0122\7\3\2\2\u0122\u0123\7\6\2\2\u0123\u0124\7)\2\2\u0124%\3\2\2\2\u0125"+
		"\u012e\7\f\2\2\u0126\u012b\5(\25\2\u0127\u0128\7\6\2\2\u0128\u012a\5("+
		"\25\2\u0129\u0127\3\2\2\2\u012a\u012d\3\2\2\2\u012b\u0129\3\2\2\2\u012b"+
		"\u012c\3\2\2\2\u012c\u012f\3\2\2\2\u012d\u012b\3\2\2\2\u012e\u0126\3\2"+
		"\2\2\u012e\u012f\3\2\2\2\u012f\u0130\3\2\2\2\u0130\u0131\7\b\2\2\u0131"+
		"\'\3\2\2\2\u0132\u0133\7\35\2\2\u0133\u0134\7)\2\2\u0134\u0135\7\r\2\2"+
		"\u0135\u013a\5\"\22\2\u0136\u0137\7\6\2\2\u0137\u0139\5\"\22\2\u0138\u0136"+
		"\3\2\2\2\u0139\u013c\3\2\2\2\u013a\u0138\3\2\2\2\u013a\u013b\3\2\2\2\u013b"+
		"\u013d\3\2\2\2\u013c\u013a\3\2\2\2\u013d\u013e\7\n\2\2\u013e)\3\2\2\2"+
		"\u013f\u0144\7)\2\2\u0140\u0141\7\6\2\2\u0141\u0143\7)\2\2\u0142\u0140"+
		"\3\2\2\2\u0143\u0146\3\2\2\2\u0144\u0142\3\2\2\2\u0144\u0145\3\2\2\2\u0145"+
		"+\3\2\2\2\u0146\u0144\3\2\2\2\u0147\u014c\5.\30\2\u0148\u0149\7\6\2\2"+
		"\u0149\u014b\5.\30\2\u014a\u0148\3\2\2\2\u014b\u014e\3\2\2\2\u014c\u014a"+
		"\3\2\2\2\u014c\u014d\3\2\2\2\u014d-\3\2\2\2\u014e\u014c\3\2\2\2\u014f"+
		"\u0150\t\2\2\2\u0150/\3\2\2\2\u0151\u0156\7\'\2\2\u0152\u0153\7\6\2\2"+
		"\u0153\u0155\7\'\2\2\u0154\u0152\3\2\2\2\u0155\u0158\3\2\2\2\u0156\u0154"+
		"\3\2\2\2\u0156\u0157\3\2\2\2\u0157\61\3\2\2\2\u0158\u0156\3\2\2\2\378"+
		"@S\\_fjpvy\u0081\u0084\u0099\u00a4\u00b1\u00ba\u00c4\u00d6\u00dd\u00ed"+
		"\u00f0\u0106\u0116\u012b\u012e\u013a\u0144\u014c\u0156";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}