/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MMO Loader.
 *
 * MMO Loader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MMO Loader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MMO Loader.  If not, see <http:www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.medline.mmo;

import edu.uvm.ccts.common.db.DataSource;
import edu.uvm.ccts.common.util.TimeUtil;
import org.apache.commons.cli.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;
import java.util.Properties;

/**
 * Created by mstorer on 5/7/14.
 */
public class Load {
    private static final Log log = LogFactory.getLog(Load.class);

    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";

    public static void main(String[] args) {
        System.out.println("MEDLINE MMO Loader");
        System.out.println("------------------");
        System.out.println("Copyright 2015 The University of Vermont and State Agricultural College.  All rights reserved.\n");

        if (args.length == 0) {
            showHelp();
            System.exit(0);
        }

        try {
            CommandLineParser parser = new BasicParser();
            CommandLine line = parser.parse(getCLIOptions(), args);

            MMOLoader loader = new MMOLoader();

            long start = System.currentTimeMillis();
            log.info("process started at " + new Date());

            if (line.hasOption("prepare")) {
                String path = line.getOptionValue("prepare");

                loader.prepare(path);
                log.info("preparing files from '" + path + "' finished at " + new Date() + " (took " +
                        TimeUtil.formatMsToHMS(System.currentTimeMillis() - start) + ").");

            } else if (line.hasOption("load")) {
                String host = line.hasOption("host") ? line.getOptionValue("host") : "localhost";
                String db = line.hasOption("db") ? line.getOptionValue("db") : "mmo";
                String user = line.hasOption("user") ? line.getOptionValue("user") : "mmo";
                String pass = line.hasOption("pass") ? line.getOptionValue("pass") : "mmo";

                String url = "jdbc:mysql://" + host + "/" + db;

                Properties properties = new Properties();
                properties.put("user", user);
                properties.put("password", pass);

                DataSource dataSource = new DataSource(db, JDBC_DRIVER, url, properties);

                loader.populateDatabase(dataSource);

                log.info("populating database finished at " + new Date() + " (took " +
                        TimeUtil.formatMsToHMS(System.currentTimeMillis() - start) + ").");
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            System.exit(-1);
        }
    }

    private static void showHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.setWidth(100);
        formatter.printHelp("load", getCLIOptions(), true);
    }

    private static Options getCLIOptions() {
        Options options = new Options();

        options.addOption(OptionBuilder.hasArg()
                .withLongOpt("host")
                .withArgName("string")
                .withDescription("the database host (default 'localhost')")
                .create('h'));
        options.addOption(OptionBuilder.hasArg()
                .withLongOpt("db")
                .withArgName("string")
                .withDescription("the database name (default 'mmo')")
                .create('d'));
        options.addOption(OptionBuilder.hasArg()
                .withLongOpt("user")
                .withArgName("string")
                .withDescription("the database user name (default 'mmo')")
                .create('u'));
        options.addOption(OptionBuilder.hasArg()
                .withLongOpt("pass")
                .withArgName("string")
                .withDescription("the database user password (default 'mmo')")
                .create('p'));

        OptionGroup group = new OptionGroup();
        group.addOption(OptionBuilder.hasArg()
                .withLongOpt("prepare")
                .withArgName("path")
                .withDescription("only prepare database files for import from <path>")
                .create());
        group.addOption(OptionBuilder.withLongOpt("load")
                .withDescription("only load previously-prepared files into the target database")
                .create());
        group.setRequired(true);
        options.addOptionGroup(group);

        return options;
    }
}
