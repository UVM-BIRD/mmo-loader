/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MMO Loader.
 *
 * MMO Loader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MMO Loader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MMO Loader.  If not, see <http:www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.medline.mmo.model;

import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * Created by mstorer on 5/7/14.
 */
public class Utterance {
    private String id;
    private int sectionId;
    private String utterance;
    private List<Phrase> phraseList;
    private List<Position> positionInfoList;
    private List<Integer> nList;


    public Utterance(String id, String utterance, List<Position> positionInfoList, List<Integer> nList) throws NoSuchAlgorithmException {
        this.id = id;
        this.utterance = utterance;
        this.positionInfoList = positionInfoList;
        this.nList = nList;

        String[] parts = id.split("\\.");        // format : "16691646.ti.1"
        sectionId = Integer.parseInt(parts[2]);
    }

    public String getId() {
        return id;
    }

    public int getSectionId() {
        return sectionId;
    }

    public String getUtterance() {
        return utterance;
    }

    public void setPhraseList(List<Phrase> phraseList) {
        this.phraseList = phraseList;
    }

    public List<Phrase> getPhraseList() {
        return phraseList;
    }

    public List<Position> getPositionInfoList() {
        return positionInfoList;
    }

    public List<Integer> getnList() {
        return nList;
    }
}
