// Generated from MMO.g4 by ANTLR 4.1
package edu.uvm.ccts.medline.mmo.antlr;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link MMOParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface MMOVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link MMOParser#phraseFeatures}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPhraseFeatures(@NotNull MMOParser.PhraseFeaturesContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#args}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgs(@NotNull MMOParser.ArgsContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#phraseLexMatch}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPhraseLexMatch(@NotNull MMOParser.PhraseLexMatchContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#strList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStrList(@NotNull MMOParser.StrListContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#negList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNegList(@NotNull MMOParser.NegListContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#phraseTag}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPhraseTag(@NotNull MMOParser.PhraseTagContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#negation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNegation(@NotNull MMOParser.NegationContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#positionList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPositionList(@NotNull MMOParser.PositionListContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#mappings}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMappings(@NotNull MMOParser.MappingsContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#phrase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPhrase(@NotNull MMOParser.PhraseContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#map}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMap(@NotNull MMOParser.MapContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#utterance}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUtterance(@NotNull MMOParser.UtteranceContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#upcm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUpcm(@NotNull MMOParser.UpcmContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#numList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumList(@NotNull MMOParser.NumListContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#candidates}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCandidates(@NotNull MMOParser.CandidatesContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#ev}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEv(@NotNull MMOParser.EvContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#phraseTokens}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPhraseTokens(@NotNull MMOParser.PhraseTokensContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#matchMap}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMatchMap(@NotNull MMOParser.MatchMapContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#init}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInit(@NotNull MMOParser.InitContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#phraseInputMatch}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPhraseInputMatch(@NotNull MMOParser.PhraseInputMatchContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#aasItem2}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAasItem2(@NotNull MMOParser.AasItem2Context ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#pcm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPcm(@NotNull MMOParser.PcmContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#aasItem3}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAasItem3(@NotNull MMOParser.AasItem3Context ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#aas}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAas(@NotNull MMOParser.AasContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#metaMapOptions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMetaMapOptions(@NotNull MMOParser.MetaMapOptionsContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#taggingInfo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTaggingInfo(@NotNull MMOParser.TaggingInfoContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#str}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStr(@NotNull MMOParser.StrContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#negatedConceptList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNegatedConceptList(@NotNull MMOParser.NegatedConceptListContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#aasItemNormal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAasItemNormal(@NotNull MMOParser.AasItemNormalContext ctx);

	/**
	 * Visit a parse tree produced by {@link MMOParser#taggingInfoList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTaggingInfoList(@NotNull MMOParser.TaggingInfoListContext ctx);
}