/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of MMO Loader.
 *
 * MMO Loader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MMO Loader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MMO Loader.  If not, see <http:www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.medline.mmo;

import edu.uvm.ccts.common.exceptions.antlr.GrammarException;
import edu.uvm.ccts.medline.mmo.antlr.MMOBaseVisitor;
import edu.uvm.ccts.medline.mmo.antlr.MMOParser;
import edu.uvm.ccts.medline.mmo.model.*;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstorer on 5/7/14.
 */
public class MMOEvalVisitor extends MMOBaseVisitor<Object> {
    private static final Log log = LogFactory.getLog(MMOEvalVisitor.class);

    private static final String YES = "yes";
    private static final String NO = "no";

    private Record record = null;

    public Record getRecord() {
        return record;
    }

    @Override
    public Object visitInit(@NotNull MMOParser.InitContext ctx) {
        Args args = (Args) visit(ctx.args());
        Aas aas = (Aas) visit(ctx.aas());
        NegList negList = (NegList) visit(ctx.negList());

        List<Utterance> utteranceList = new ArrayList<Utterance>();
        for (MMOParser.UpcmContext upcmCtx : ctx.upcm()) {
            utteranceList.add((Utterance) visit(upcmCtx));
        }

        try {
            record = new Record(args, aas, negList, utteranceList);

        } catch (Exception e) {
            if (e instanceof RuntimeException) {
                throw (RuntimeException) e;

            } else {
                throw new RuntimeException(e);
            }
        }

        return null;
    }

    @Override
    public Object visitUpcm(@NotNull MMOParser.UpcmContext ctx) {
        Utterance utterance = (Utterance) visit(ctx.utterance());

        List<Phrase> phraseList = new ArrayList<Phrase>();
        for (MMOParser.PcmContext pcmCtx : ctx.pcm()) {
            phraseList.add((Phrase) visit(pcmCtx));
        }

        utterance.setPhraseList(phraseList);

        return utterance;
    }

    @Override
    public Object visitPcm(@NotNull MMOParser.PcmContext ctx) {
        Phrase phrase = (Phrase) visit(ctx.phrase());
        phrase.setCandidates((Candidates) visit(ctx.candidates()));
        phrase.setMappings((Mappings) visit(ctx.mappings()));

        return phrase;
    }


///////////////////////////////////////////////////////////
// args
//

    @Override
    @SuppressWarnings("unchecked")
    public Object visitArgs(@NotNull MMOParser.ArgsContext ctx) {
        String commandLineCall = fixQuotesAndEscaping(ctx.Str().getText());
        List<MetaMapOption> metaMapOptions = (List<MetaMapOption>) visit(ctx.metaMapOptions());

        return new Args(commandLineCall, metaMapOptions);
    }

    @Override
    public Object visitMetaMapOptions(@NotNull MMOParser.MetaMapOptionsContext ctx) {
        List<MetaMapOption> list = new ArrayList<MetaMapOption>();
        for (TerminalNode tn : ctx.MetaMapOption()) {
            String text = tn.getText();

            int pos = text.indexOf('-');
            String name = text.substring(0, pos);
            String value = fixQuotesAndEscaping(text.substring(pos + 1));

            list.add(new MetaMapOption(name, value));
        }
        return list;
    }


///////////////////////////////////////////////////////////
// aas
//

    @Override
    public Object visitAas(@NotNull MMOParser.AasContext ctx) {
        List<AasItem> list = new ArrayList<AasItem>();
        for (MMOParser.AasItemContext itemCtx : ctx.aasItem()) {
            list.add((AasItem) visit(itemCtx));
        }
        return new Aas(list);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object visitAasItemNormal(@NotNull MMOParser.AasItemNormalContext ctx) {
        AasItem aasItem = buildBaseAasItem(ctx.AASAcronymExpansion().getText());

        List<Integer> countList = (List<Integer>) visit(ctx.numList());
        List<String> cuiList = ctx.strList() != null ?
                (List<String>) visit(ctx.strList()) :
                new ArrayList<String>();

        aasItem.setCountList(countList);
        aasItem.setCuiList(cuiList);

        return aasItem;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object visitAasItem2(@NotNull MMOParser.AasItem2Context ctx) {
        AasItem aasItem = buildBaseAasItem(ctx.AASAcronymExpansion2().getText());

        // there is no count list for this variation.  in its place is a third string element,
        // e.g. "^J^I^K$" - whatever that means.  only exists for a couple records out of thousands.  it is also
        // undocumented, not in the official definition, such that there is.

        List<String> cuiList = ctx.strList() != null ?
                (List<String>) visit(ctx.strList()) :
                new ArrayList<String>();

        aasItem.setCuiList(cuiList);

        return aasItem;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object visitAasItem3(@NotNull MMOParser.AasItem3Context ctx) {
        AasItem aasItem = buildBaseAasItem(ctx.AASAcronymExpansion3().getText());

        // this variation starts showing up in the 2015 data set.

        List<String> cuiList = ctx.strList() != null ?
                (List<String>) visit(ctx.strList()) :
                new ArrayList<String>();

        aasItem.setCuiList(cuiList);

        return aasItem;
    }

    private AasItem buildBaseAasItem(String s) {
        List<String> parts = new ArrayList<String>();
        for (String part : s.split("\\*")) {
            parts.add(trimQuotesIfPresent(part));
        }

        String acronym = parts.get(0);
        String expansion = parts.get(1);

        return new AasItem(acronym, expansion);
    }

    private String trimQuotesIfPresent(String s) {
        if ((s.startsWith("'") && s.endsWith("'")) || (s.startsWith("\"") && s.endsWith("\""))) {
            return s.substring(1, s.length() - 1);
        } else {
            return s;
        }
    }


///////////////////////////////////////////////////////////
// negation list
//

    @Override
    public Object visitNegList(@NotNull MMOParser.NegListContext ctx) {
        List<Negation> list = new ArrayList<Negation>();
        for (MMOParser.NegationContext nCtx : ctx.negation()) {
            list.add((Negation) visit(nCtx));
        }
        return new NegList(list);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object visitNegation(@NotNull MMOParser.NegationContext ctx) {
        String type = ctx.Word().getText();
        String trigger = (String) visit(ctx.str());
        List<Position> triggerPositionInfo = (List<Position>) visit(ctx.positionList(0));
        List<NegatedConcept> negatedConcepts = (List<NegatedConcept>) visit(ctx.negatedConceptList());
        List<Position> conceptPositionInfo = (List<Position>) visit(ctx.positionList(1));

        return new Negation(type, trigger, triggerPositionInfo, negatedConcepts, conceptPositionInfo);
    }

    @Override
    public Object visitNegatedConceptList(@NotNull MMOParser.NegatedConceptListContext ctx) {
        List<NegatedConcept> list = new ArrayList<NegatedConcept>();
        for (TerminalNode tn : ctx.NegatedConcept()) {
            list.add(parseNegatedConcept(tn.getText()));
        }
        return list;
    }

    private NegatedConcept parseNegatedConcept(String s) {
        int pos = s.indexOf(':');
        String key = fixQuotesAndEscaping(s.substring(0, pos));
        String value = fixQuotesAndEscaping(s.substring(pos + 1));
        return new NegatedConcept(key, value);
    }


///////////////////////////////////////////////////////////
// utterance
//

    @Override
    @SuppressWarnings("unchecked")
    public Object visitUtterance(@NotNull MMOParser.UtteranceContext ctx) {
        String id = (String) visit(ctx.str());
        String sentence = fixQuotesAndEscaping(ctx.DQStr().getText());
        List<Position> positionInfo = (List<Position>) visit(ctx.positionList());
        List<Integer> nList = ctx.numList() != null ?
                (List<Integer>) visit(ctx.numList()) :
                new ArrayList<Integer>();

        try {
            return new Utterance(id, sentence, positionInfo, nList);

        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }


///////////////////////////////////////////////////////////
// phrase
//

    @Override
    @SuppressWarnings("unchecked")
    public Object visitPhrase(@NotNull MMOParser.PhraseContext ctx) {
        String phrase = (String) visit(ctx.str());
        List<TaggingInfo> taggingInfo = (List<TaggingInfo>) visit(ctx.taggingInfoList());
        List<Position> positionInfo = (List<Position>) visit(ctx.positionList());
        List<Integer> nList = ctx.numList() != null ?
                (List<Integer>) visit(ctx.numList()) :
                new ArrayList<Integer>();

        return new Phrase(phrase, taggingInfo, positionInfo, nList);
    }

    @Override
    public Object visitTaggingInfoList(@NotNull MMOParser.TaggingInfoListContext ctx) {
        List<TaggingInfo> list = new ArrayList<TaggingInfo>();
        for (MMOParser.TaggingInfoContext tiCtx : ctx.taggingInfo()) {
            list.add((TaggingInfo) visit(tiCtx));
        }
        return list;
    }

    @Override
    public Object visitTaggingInfo(@NotNull MMOParser.TaggingInfoContext ctx) {
        String function = ctx.Word().getText().toUpperCase();

        String lexMatch = null;
        String inputMatch = null;
        String tag = null;
        String tokens = null;
        String features = null;

        for (MMOParser.TagPropertyContext tpCtx : ctx.tagProperty()) {
            TagProperty tp = (TagProperty) visit(tpCtx);
            switch (tp.getType()) {
                case LEXMATCH:      lexMatch = tp.getValue();   break;
                case INPUTMATCH:    inputMatch = tp.getValue(); break;
                case TAG:           tag = tp.getValue();        break;
                case TOKENS:        tokens = tp.getValue();     break;
                case FEATURES:      features = tp.getValue();   break;
                default:            throw new GrammarException("unexpected grammar for tagging info");
            }
        }

        return new TaggingInfo(function, lexMatch, inputMatch, tag, tokens, features);
    }

    @Override
    public Object visitPhraseLexMatch(@NotNull MMOParser.PhraseLexMatchContext ctx) {
        String value = (String) visit(ctx.str());
        return new TagProperty(TagPropertyType.LEXMATCH, value);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object visitPhraseInputMatch(@NotNull MMOParser.PhraseInputMatchContext ctx) {
        List<String> list = (List<String>) visit(ctx.strList());

        String value = StringUtils.join(list, "/");

        return new TagProperty(TagPropertyType.INPUTMATCH, value);
    }

    @Override
    public Object visitPhraseTag(@NotNull MMOParser.PhraseTagContext ctx) {
        String value = ctx.Word().getText();
        return new TagProperty(TagPropertyType.TAG, value);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object visitPhraseTokens(@NotNull MMOParser.PhraseTokensContext ctx) {
        String value = null;

        if (ctx.strList() != null) {
            List<String> list = (List<String>) visit(ctx.strList());
            value = StringUtils.join(list, "/");
        }

        return new TagProperty(TagPropertyType.TOKENS, value);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object visitPhraseFeatures(@NotNull MMOParser.PhraseFeaturesContext ctx) {
        String value = null;

        if (ctx.strList() != null) {
            List<String> list = (List<String>) visit(ctx.strList());
            value = StringUtils.join(list, "/");
        }

        return new TagProperty(TagPropertyType.FEATURES, value);
    }


///////////////////////////////////////////////////////////
// candidates
//

    @Override
    public Object visitCandidates(@NotNull MMOParser.CandidatesContext ctx) {
        Integer n1 = Integer.parseInt(ctx.N(0).getText());
        Integer n2 = Integer.parseInt(ctx.N(1).getText());
        Integer n3 = Integer.parseInt(ctx.N(2).getText());
        Integer n4 = Integer.parseInt(ctx.N(3).getText());

        List<EV> list = new ArrayList<EV>();
        for (MMOParser.EvContext evCtx : ctx.ev()) {
            list.add((EV) visit(evCtx));
        }

        return new Candidates(n1, n2, n3, n4, list);
    }


///////////////////////////////////////////////////////////
// ev
//

    @Override
    @SuppressWarnings("unchecked")
    public Object visitEv(@NotNull MMOParser.EvContext ctx) {
        Integer score = Integer.parseInt(ctx.N(0).getText());
        String umlsId = (String) visit(ctx.str(0));
        String concept = (String) visit(ctx.str(1));
        String prefName = (String) visit(ctx.str(2));
        List<String> matchedWords = (List<String>) visit(ctx.strList(0));
        List<String> semanticTypes = (List<String>) visit(ctx.strList(1));

        List<MatchMap> matchMapList = new ArrayList<MatchMap>();
        for (MMOParser.MatchMapContext mmCtx : ctx.matchMap()) {
            matchMapList.add((MatchMap) visit(mmCtx));
        }

        Boolean involvedWithHead = convertToBoolean(ctx.Word(0).getText());
        Boolean overmatch = convertToBoolean(ctx.Word(1).getText());

        List<String> sourceList = (List<String>) visit(ctx.strList(2));
        List<Position> positionList = (List<Position>) visit(ctx.positionList());
        Integer n1 = Integer.parseInt(ctx.N(1).getText());

        return new EV(score, umlsId, concept, prefName, matchedWords, semanticTypes, matchMapList,
                involvedWithHead, overmatch, sourceList, positionList, n1);
    }

    @Override
    public Object visitMatchMap(@NotNull MMOParser.MatchMapContext ctx) {
        Integer phraseSpanBegin =   Integer.parseInt(ctx.N(0).getText());
        Integer phraseSpanEnd =     Integer.parseInt(ctx.N(1).getText());
        Integer conceptSpanBegin =  Integer.parseInt(ctx.N(2).getText());
        Integer conceptSpanEnd =    Integer.parseInt(ctx.N(3).getText());
        Integer variation =         Integer.parseInt(ctx.N(4).getText());

        return new MatchMap(phraseSpanBegin, phraseSpanEnd, conceptSpanBegin, conceptSpanEnd, variation);
    }

    private Boolean convertToBoolean(String s) {
        if (YES.equalsIgnoreCase(s))     return true;
        else if (NO.equalsIgnoreCase(s)) return false;
        else                             throw new GrammarException("unexpected grammar for boolean");
    }


///////////////////////////////////////////////////////////
// mappings
//

    @Override
    public Object visitMappings(@NotNull MMOParser.MappingsContext ctx) {
        List<MapInfo> list = new ArrayList<MapInfo>();
        for (MMOParser.MapContext mCtx : ctx.map()) {
            list.add((MapInfo) visit(mCtx));
        }
        return new Mappings(list);
    }

    @Override
    public Object visitMap(@NotNull MMOParser.MapContext ctx) {
        Integer score = Integer.parseInt(ctx.N().getText());
        List<EV> list = new ArrayList<EV>();
        for (MMOParser.EvContext evCtx : ctx.ev()) {
            list.add((EV) visit(evCtx));
        }
        return new MapInfo(score, list);
    }


///////////////////////////////////////////////////////////
// misc
//

    @Override
    public Object visitStrList(@NotNull MMOParser.StrListContext ctx) {
        List<String> list = new ArrayList<String>();
        for (MMOParser.StrContext socCtx : ctx.str()) {
            list.add((String) visit(socCtx));
        }
        return list;
    }

    @Override
    public Object visitStr(@NotNull MMOParser.StrContext ctx) {
        if (ctx.Str() != null)          return fixQuotesAndEscaping(ctx.Str().getText());
        else if (ctx.Word() != null)    return ctx.Word().getText();
        else                            throw new GrammarException("unexpected grammar for str");
    }

    @Override
    public Object visitPositionList(@NotNull MMOParser.PositionListContext ctx) {
        List<Position> list = new ArrayList<Position>();
        for (TerminalNode tn : ctx.Position()) {
            list.add(parsePosition(tn.getText()));
        }
        return list;
    }

    private Position parsePosition(String s) {
        int pos = s.indexOf('/');
        Integer startPos = Integer.parseInt(s.substring(0, pos));
        Integer spanLength = Integer.parseInt(s.substring(pos + 1));
        return new Position(startPos, spanLength);
    }

    @Override
    public Object visitNumList(@NotNull MMOParser.NumListContext ctx) {
        List<Integer> list = new ArrayList<Integer>();
        for (TerminalNode tn : ctx.N()) {
            list.add(Integer.parseInt(tn.getText()));
        }
        return list;
    }


////////////////////////////////////////////////////////////////////////////
// private methods
//

    private String fixQuotesAndEscaping(String s) {
        if (s == null) return null;

        if (s.startsWith("'") && s.endsWith("'")) {
            s = s.substring(1, s.length() - 1);
            if (s.contains("\\'")) {
                s = s.replaceAll("\\\\'", "'");
            }

        } else if (s.startsWith("\"") && s.endsWith("\"")) {
            s = s.substring(1, s.length() - 1);
            if (s.contains("\\\"")) {
                s = s.replaceAll("\\\\\"", "\"");
            }
        }

        return s;
    }
}
